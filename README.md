# weblate

This repository contains Weblate-related code and was initially imported from
the [Tails Puppet module](https://gitlab.tails.boum.org/tails/puppet-tails.git).

## Upgrading

Follow these steps to upgrade Weblate to a newer version:

1. Update the container image version:

   - Make sure to check the Weblate upgrade notes, as there may be restrictions
     on which version you can upgrade to:

       https://docs.weblate.org/en/latest/admin/upgrade.html

   - Make sure to update at least `.gitlab-ci.yaml`, `manifests/params.pp` and
     any test under the `spec/` directory that could be affected. Grepping for
     the current version number can help.

2. Check whether our custom settings in `files/config/settings-override.py`
   still make sense, as some settings may have been removed in newer versions.

3. Push a feature branch to GitLab, check CI results, and fix any problems.

4. Schedule some downtime and inform users at `tails-l10n@boum.org`.

5. Save a copy of important data, for the sake of it:

   - the Weblate database:

     ```
     sudo -u postgres pg_dump weblate | bzip2 > ~/weblate_( date -I ).sql.bz2
     ```

   - the Weblate repositories:

     ```
     sudo -u weblate /var/lib/weblate/scripts/create-snapshot.sh
     ```

6. Avoid deploying the upgrade during the integration scripts cron job runs, as
   our scripts will try to update Weblate components via the Python API while
   the system is being upgraded.

7. Push the changes to production and run Puppet in the Weblate node.

## Machine Translation

Since Weblate 4.13, "Machine Translation" must be configured using the web
interface. See:

    https://docs.weblate.org/en/weblate-5.2.1/admin/machine.html
