#!/usr/bin/env python3

import collections
import git
import logging
import logging.config
import pathlib
import os

import tailsWeblate
import merge_canonical_changes as mcc


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASEPATH = "wiki/src"

logger = logging.getLogger(__name__)


def deleteComponent(fpath):
    if not mcc.isWikiPo(fpath):
        logger.debug("deleted file (ignoring): {}".format(fpath))
        return
    if mcc.getLang(fpath) != mcc.config.defaultlang:
        logger.debug("deleted file (ignoring, wrong lang): {}".format(fpath))
        return
    logger.info("deleted component: {}".format(fpath))
    subproject = tailsWeblate.subProject(fpath)
    if not subproject.repo.startswith("weblate://"):
        raise tailsWeblate.ComponentExceptionWeblatUrl(tailsWeblate.filemask(fpath), subproject)
    subproject.delete()


def renameComponent(source, target, remote_master):
    if not mcc.isWikiPo(target):
        logger.debug("renamed file (ignoring): {} -> {}".format(source, target))
        return
    if mcc.getLang(target) != mcc.config.defaultlang:
        logger.debug("renamed file (ignoring, wrong lang): {} -> {}".format(source, target))
        return

    logger.info("renamed component: {} -> {}".format(source, target))

    try:
        tailsWeblate.renameComponent(source, target)
    except tailsWeblate.TailsWeblateException as e:
        logger.warning("\t... %s, skipping.", e)


def addComponent(fpath, remote_master):
    if not mcc.isWikiPo(fpath):
        logger.debug("added file (ignoring): {}".format(fpath))
        return
    if mcc.getLang(fpath) != mcc.config.defaultlang:
        logger.debug("added file (ignoring, wrong lang): {}".format(fpath))
        return
    logger.info("added component: {}".format(fpath))

    try:
        tailsWeblate.addComponent(fpath)
    except tailsWeblate.TailsWeblateException as e:
        logger.warning("\t... %s, skipping.", e)


def updateComponent(fpath):
    if not mcc.isWikiPo(fpath):
        logger.debug("modified file (ignoring): {}".format(fpath))
        return
    logger.info("modified file: {}".format(fpath))
    return tailsWeblate.subProject(fpath)


def perform_update(oldHead, newHead, remoteBranch):

    logger.info("update {}..{}".format(oldHead.hexsha, newHead.hexsha))

    # create a diff
    # if there more than l files changed, git disables the detection of renames, as the algorithm is O(n^2)
    # but we need this, otherwise we do not detect renames (see man git-diff)
    diff = oldHead.diff(newHead, BASEPATH, l=10000)

    # Handle deleted files
    for deleted_file in diff.iter_change_type('D'):
        fpath = deleted_file.a_path
        try:
            deleteComponent(fpath)
        except tailsWeblate.ComponentException as e:
            logger.warning("\t... {}".format(e))
        except:
            logger.exception("Deletion triggered by '{}' failed:".format(fpath))
            raise

    # Handle renamed files
    for renamed_file in diff.iter_change_type('R'):
        try:
            source = renamed_file.rename_from
            target = renamed_file.rename_to
            renameComponent(source, target, remoteBranch)
        except tailsWeblate.ComponentException as e:
            logger.warning("\t... {}".format(e))
        except:
            logger.exception("Rename triggered by '{}'->'{}' failed:".format(source, target))
            raise

    # Handle added files
    for added_file in diff.iter_change_type('A'):
        fpath = added_file.a_path
        try:
            addComponent(fpath, remoteBranch)
        except:
            logger.exception("Adding new component triggered by '{}' failed:".format(fpath))
            raise

    # Handle modified files
    modifiedComponents = dict()
    loadpo = collections.defaultdict(list)
    for modified_file in diff.iter_change_type('M'):
        try:
            fpath = modified_file.a_path
            subproject = updateComponent(fpath)
        except tailsWeblate.ComponentException as e:
            logger.warning("\t... {}".format(e))
        except:
            logger.exception("Updating component triggered by '{}' failed:".format(fpath))
            raise
        else:
            if subproject:
                modifiedComponents[subproject.name] = subproject
                loadpo[subproject.name].append(mcc.getLang(fpath))

    # replaces manage.py loadpo call
    for name, langs in loadpo.items():
        subproject = modifiedComponents[name]
        logger.info("update translations for: {}".format(name))
        subproject.create_translations(force=False, langs=langs)

@tailsWeblate.lock_repository
def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--fetch",
        action='store_true',
        help="fetch and merge before start.")
    parser.add_argument(
        "--remoteBranch",
        default="origin/master",
        help="branch name to merge with syntax like: 'remote/branch'.")
    parser.add_argument(
        "--local",
        default="master",
        help="name of local branch.")
    parser.add_argument(
        "--start",
        help="start to search for updates. (either a commit hash or a tagname)")
    parser.add_argument(
        "--end",
        help="commit to stop search for updates. (either a commit hash or a tagname)")
    parser.add_argument(
        "repoPath",
        type=pathlib.Path,
        help="Path to the git repository")
    args = parser.parse_args()

    repoPath = args.repoPath
    remoteBranch = args.remoteBranch
    repo = git.Repo(str(repoPath))

    remote = repo.refs[remoteBranch]

    local = repo.refs[args.local]

    if args.start:
        try:
            oldHead = repo.refs[args.start].commit
        except IndexError:
            oldHead = repo.commit(args.start)
    else:
        oldHead = local.commit

    oldcommit = oldHead

    if args.fetch:
        # use the remote commit as base for updating the components,
        # as master branch may have updated parts of remote already partly.
        # Let's make sure everything is in sync with remote.
        oldcommit = remote.commit

        _r= repo.remotes[remote.remote_name]

        # fetch first, to see if we need to update
        _r.fetch()

        if oldcommit != remote.commit:
            logger.info("Updated remote {}..{}".format(oldcommit, remote.commit))
            try:
                _r.pull(local,ff_only=True)
                logger.info("merged remote into {} with fast-forward strategy.".format(local.name))
            except git.exc.GitCommandError:
                remote.commit = oldcommit
                logger.error("Couldn't update {} with fast-forward strategy. Reset remote to {} again".format(local.name, oldcommit))
                raise
        else:
            # remote branch is up-to-date.
            # We may have commits on top, that haven't synced back yet,
            # but those are in sync with Weblate, that's why we use the oldHead
            oldcommit = oldHead

    if args.end:
        try:
            newHead = repo.refs[args.end].commit
        except IndexError:
            newHead = repo.commit(args.end)
    else:
        newHead = local.commit

    if oldcommit != newHead:
        logger.debug("Updating components {}..{}".format(oldcommit, newHead))
        perform_update(oldcommit, newHead, remote)
        logger.info("-- Finished successfully. --")
    else:
        logger.info("-- Head not changed - nothing to do. --")


if __name__ == "__main__":
    # Setup logging
    logging.config.fileConfig(SCRIPT_DIR + '/../config/updateWeblateComponents.conf')
    logger = logging.getLogger('')
    mcc.logger = logging.getLogger('mcc')

    try:
        main()
    except:
        logger.exception("-- Something unexpected happened. Giving up. --")
        raise
