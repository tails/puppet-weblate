#!/usr/bin/env python3

"""Script to merge into Weblate Git (local) from main Git (remote).
Rules about what Weblate is allowed to do and how merge conflicts should
be handled:
 * PO files only modified locally - local version is used.
 * PO files modifed remotely and locally are merged with @mergePo:
   - mergePo understands the PO file format and does a merge based on
     the PO msgids.
   - If there is a merge conflict on one msgid (remote and local modified
     the content for the same msgid), the remote version is preferred.
     This makes sure that Tails developers can fix issues in translations
     over Weblate.
 * Otherwise, remote is preferred.
"""

import git

import copy
import json
import logging
import logging.config
import os
import re
import subprocess
import tempfile
import pathlib


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

logger = logging.getLogger(__name__)
parser = None
prog = 'merge_canonical_changes'
temporaryDirectory = lambda x: tempfile.TemporaryDirectory()


class ConfigNotFoundError(Exception):
    pass


class Config:
    def __init__(self, paths):
        self.paths = paths
        self.config = None

    def load(self):
        for path in self.paths:
            if path.exists():
                with path.open() as f:
                    self.config = json.load(f)
                break
        else:
            self.config = None
            raise ConfigNotFoundError("No config was found, searched in: %s", self.paths)

    def __getattr__(self, item):
        if not self.config:
            self.load()
        return self.config.get(item)


path = pathlib.Path(os.path.realpath(__file__))
config = Config([pathlib.Path('.langs.json'), path.with_name('langs.json'), path.parent.with_name("config")/'langs.json'])
del(path)

# MAINGITLANGS = config.mainlangs
# DEFAULTLANG = config.defaultlang
# ADDITONALLANGS = config.additionallangs


def logInputError(method, msg):
    """If we want to log an input error and a parser is availabe use parser to split out error."""
    if parser:
        _logger = parser
    else:
        _logger = logger

    getattr(_logger, 'error')(msg)


def isWikiPo(_path):
    if isinstance(_path, pathlib.Path):
        path = _path
    else:
        path = pathlib.Path(_path)
    if path.parts[:2] == ("wiki", "src") and path.name.endswith(".po"):
        return True
    return False


class NoPropperPoFile(Exception):
    pass


def getLang(_path):
    """returns language of path"""
    if isinstance(_path, pathlib.Path):
        path = _path
    else:
        path = pathlib.Path(_path)
    try:
        *name, lang, ext = path.name.split(".")
    except ValueError as e:
        raise NoPropperPoFile(_path) from e
    if not name:
        raise NoPropperPoFile(_path)
    if ext != "po":
        raise NoPropperPoFile(_path)
    return lang


class Index:
    """class to handle a git index"""

    def __init__(self, index):
        self.index = index
        # conflicting files
        self.unmerged_blob_map = index.unmerged_blobs()
        # local modified files
        self.local_modified_files = set()

    def removeFromUnmergedBlobMap(self, path):
        del(self.unmerged_blob_map[path])

    def removeFile(self, path):
        """removes path from index"""
        for stage in (0, 1, 2, 3):
            try:
                del(self.index.entries[(path, stage)])
            except KeyError:
                pass

    def updateFile(self, path, blob):
        """updates path with blob"""
        if path in self.local_modified_files:
            self.local_modified_files.remove(path)

        if path in self.unmerged_blob_map.keys():
            self.index.resolve_blobs([blob])
            self.removeFromUnmergedBlobMap(path)
        else:
            self.add([blob])

    def add(self, elements):
        """add elements to index"""
        return self.index.add(elements)


# ------ functions to manage a po merge ------

def pot(blob):
    """create pot file out of git blob"""
    with tempfile.NamedTemporaryFile(suffix=".po") as temp:
        blob.stream_data(temp)
        temp.flush()
        podebug = subprocess.run(['/usr/local/bin/podebug', '--progress=none', '--rewrite=blank', temp.name],
                                 stdout=subprocess.PIPE,
                                 check=True)
        return subprocess.check_output(['msgattrib', '--no-obsolete'], input=podebug.stdout)


def msguniq(blob, fname):
    """makes msgid unique for content of git blob and store the output in @fname."""
    return subprocess.run(['msguniq', '-o', str(fname), '--force-po'],
                          input=blob.data_stream.read(),
                          check=True)


def extractChanges(fileA, base):
    """messages changed between fileA and base."""
    msgcat = subprocess.run(['msgcat', '-o', '-', str(fileA), str(base)],
                            stdout=subprocess.PIPE,
                            check=True)
    msggrep = subprocess.run(['msggrep', '--msgstr', '-F', '-e', '#-#-#-#-#', '-'],
                             input=msgcat.stdout,
                             stdout=subprocess.PIPE,
                             check=True)
    msgmerge = subprocess.run(['msgmerge', '--force-po', '--quiet', '-o', '-', str(fileA), '-'],
                              input=msggrep.stdout,
                              stdout=subprocess.PIPE,
                              check=True)
    graveyeards = re.compile(b'^#~.*\n', re.M)
    return graveyeards.sub(b'', msgmerge.stdout)


def unchangedChanges(base, local, remote):
    """message that have not changed."""
    msgcat = subprocess.run(['msgcat', '-o', '-', str(base), str(local), str(remote)],
                            stdout=subprocess.PIPE,
                            check=True)
    msggrep = subprocess.run(['msggrep', '-v', '--msgstr', '-F', '-e', '#-#-#-#-#', '-'],
                             input=msgcat.stdout,
                             stdout=subprocess.PIPE,
                             check=True)
    return msggrep.stdout


def poHeader(poFileContent):
    """@returns the po header of the poFile."""
    m = re.search(b'^$', poFileContent, re.M)
    return poFileContent[:m.start()-1]


def mergePo(origPath, lang, output):
    """merges po files.
    it expects, that @origPath is a directory with following files (pot, local, base and remote)
    returns the merge content to the file @output."""
    pot = origPath/'pot'
    local = origPath/'local'
    base = origPath/'base'
    remote = origPath/'remote'

    with temporaryDirectory(origPath/"merge") as tempdir:
        path = pathlib.Path(tempdir)
        header = path/'header'
        localChanges = path/'localChanges'
        remoteChanges = path/'remoteChanges'
        unchanged = path/'unchanged'

        # get only header of pot file
        header.write_bytes(poHeader(pot.read_bytes()))

        # messages changed on local/remote
        localChanges.write_bytes(extractChanges(local, base))
        remoteChanges.write_bytes(extractChanges(remote, base))

        # unchanged messages
        unchanged.write_bytes(unchangedChanges(base, local, remote))

        # messages changed on local, not on remote; and vice-versa
        unique = subprocess.run(['msgcat', '-o', '-', '--unique', str(localChanges), str(remoteChanges)],
                                stdout=subprocess.PIPE,
                                check=True)

        # the big merge (unchanged, unique and remoteChanges)
        # Keep in mind, that we DO NOT add localChanges as remoteChanges are preferred
        # over local changes for tails this is fine as everything form main git is
        # preferred over weblate and we would create conflicts in po files

        msgcat = subprocess.run(['msgcat', '-o', '-', str(unchanged), '-', str(remoteChanges)],
                                input=unique.stdout,
                                stdout=subprocess.PIPE,
                                check=True)

        # filter messages actually needed (those on local and remote)
        msgmerge = subprocess.run(['msgmerge', '--force-po', '--quiet', '--no-fuzzy-matching', '-o', '-', '-', str(pot)],
                                  input=msgcat.stdout,
                                  stdout=subprocess.PIPE,
                                  check=True)

        # final merge, adds saved header
        subprocess.run(['msgcat', "--lang", lang, '-w', '79', '-o', str(output), '--use-first', str(header), '-'],
                       input=msgmerge.stdout,
                       check=True)


# ------ functions to manage non main git files ------

def runForNonMainGit(basepath, tree, func):
    """iterates over all non maingit po files for basepath in tree.
        for each matching file func is executed with (path, lang, blob).
    """
    if type(basepath) == str:
        basepath = pathlib.Path(basepath)
    *name, _, _ = basepath.name.split('.')
    base = basepath.with_name("\.".join(name))
    regex = re.compile("^{}\.[^.]+\.po$".format(base))
    if basepath.parent != pathlib.Path("."):
        try:
            tree /= str(basepath.parent)
        except KeyError:
            # Tree is not existent, so we can't do anything
            return
    for blob in tree:
        path = blob.path
        if not regex.match(path):
            continue
        lang = getLang(path)
        if lang in config.mainlangs:
            continue
        func(path, lang, blob)


def addNonMainGitLangs(path, remotepot, repopath, index, overwriteExisting=True):
    """Adds all non maingit language files for path."""
    if type(path) == str:
        path = pathlib.Path(path)

    *name, _, _ = path.name.split('.')
    base = path.with_name(".".join(name))

    msgs = []
    with temporaryDirectory(path) as tempdir:
        temppath = pathlib.Path(tempdir)
        pot = (temppath/'pot')
        pot.write_bytes(remotepot)
        for lang in config.additionallangs:
            newpath = "{base}.{lang}.po".format(base=base, lang=lang)
            if not overwriteExisting and (repopath/newpath).exists():
                logger.info("Skipping to create %s as it already exists.", newpath)
                continue
            m = "{newpath}: adding as {path} has added.".format(path=path, newpath=newpath)
            logger.debug(m)

            ((repopath/newpath).parent).mkdir(parents=True, exist_ok=True)
            # update against pot
            subprocess.run(['msgmerge', '--force-po', '--lang', lang, '-w', '79', '--quiet', '-o', str(repopath/newpath), str(pot), str(pot)],
                           check=True)

            index.add([newpath])
            msgs.append(m)

    msgs.append("")
    return "\n".join(msgs)


def unlinkNonMainGitLangs(basepath, tree, index):
    """unlink all non maingit language files for basepath."""
    msgs = []

    def func(path, lang, blob):
        m = "{path}: unlink, as {basepath} has unlinked.".format(path=path, basepath=basepath)
        logger.debug(m.strip())
        index.removeFile(path)

        msgs.append(m)

    runForNonMainGit(basepath, tree, func)

    msgs.append("")
    return "\n".join(msgs)


def moveNonMainGitLangs(basepath, newpath, tree, repopath, index):
    """move all non maingit language files from basepath to newpath."""
    if type(newpath) == str:
        newpath = pathlib.Path(newpath)

    *name, _, _ = newpath.name.split('.')
    newbase = newpath.with_name(".".join(name))

    msgs = []

    def func(path, lang, blob):
        newpath = "{newbase}.{lang}.po".format(newbase=newbase, lang=lang)

        m = "{path}: moving -> {newpath}, as {basepath} has moved.".format(path=path, newpath=newpath, basepath=basepath)
        logger.debug(m.strip())
        index.removeFile(path)
        os.makedirs(str((repopath/newpath).parent), exist_ok=True)
        (repopath/newpath).write_bytes(blob.data_stream.read())
        index.add([newpath])

        msgs.append(m)

    runForNonMainGit(basepath, tree, func)

    msgs.append("")
    return "\n".join(msgs)


def updateNonMainGitLangs(source, target, potFile, tree, repopath, index):
    """update all non maingit language files from basepath against the potFile."""
    if type(target) == str:
        target = pathlib.Path(target)

    *name, _, _ = target.name.split('.')
    newbase = target.with_name(".".join(name))

    msgs = []

    def func(path, lang, blob):
        m = "{path}: updating as {target} has changed.".format(path=path, target=target)
        newpath = "{newbase}.{lang}.po".format(newbase=newbase, lang=lang)
        logger.debug(m.strip())
        with temporaryDirectory(path) as tempdir:
            temppath = pathlib.Path(tempdir)
            local = temppath/'local'
            msguniq(tree/path, local)
            # update against pot
            subprocess.run(['msgmerge', '--force-po', '--lang', lang, '-w', '79', '--quiet', '-o', str(repopath/newpath), str(local), '-'],
                           input=potFile,
                           check=True)

        index.removeFile(path)
        index.add([newpath])
        msgs.append(m)

    runForNonMainGit(source, tree, func)
    msgs.append("")
    return "\n".join(msgs)


def main(repopath, local, localCommit, remote):
    r = git.Repo(str(repopath))
    msg = "merge with main git using {}.\n\n".format(prog)

    try:
        local = r.refs[local]
    except IndexError:
        logInputError('{} is not a valid git reference.'.format(local))

    if localCommit:
        try:
            local.commit = r.commit(localCommit)
        except git.BadName:
            logInputError('{} is not a valid git reference.'.format(local))

    local.checkout()

    logger.info("local: %s", local.commit)
    oldcommit = local.commit

    remoteBranch = None
    try:
        remote = r.refs[remote]
        remoteBranch = remote
        remote = remote.commit
    except IndexError:
        try:
            remote = r.commit(remote)
        except git.BadName:
            logInputError('{} is not a valid git reference or a commit.'.format(remote))

    logger.info("remote: %s", remote)

    if local.commit == remote:
        logger.info('remote and local are on the same commit, nothing to do.')
        return

    # get common ancestor
    base = r.merge_base(local, remote)
    logger.info("base: %s", base[0])

    # is a fast-forward possible
    ff_possible_local = (base[0] == local.commit)    # remote branch has commits on top of local
    local_uptodate = (base[0] == remote)             # local has commits on top of remote branch
    # create index to prepare the merge
    index = Index(git.IndexFile.from_tree(r, base, local, remote))

    try:
        # A set of all local modified files
        local_modified_files = index.local_modified_files

        # Process all remote modified files
        basediff = base[0].diff(remote, l=10000)

        for i in base[0].diff(local.commit):
            path = i.b_path
            if i.change_type == "D":
                path = i.a_path
            local_modified_files.add(path)

        for i in basediff.iter_change_type('D'):
            path = i.a_path
            if path in local_modified_files:
                local_modified_files.remove(path)
            index.removeFile(path)
            if isWikiPo(path) and getLang(path) == config.defaultlang:
                msg += unlinkNonMainGitLangs(path, local.commit.tree, index)

            try:
                index.removeFromUnmergedBlobMap(path)
            except KeyError:
                pass

        for i in basediff.iter_change_type('R'):
            path = i.a_path
            index.removeFile(path)
            if isWikiPo(path) and getLang(path) == config.defaultlang:
                remotepot = pot(remote.tree/i.b_path)
                msg += moveNonMainGitLangs(path, i.b_path, local.commit.tree, repopath, index)
                msg += addNonMainGitLangs(i.b_path, remotepot, repopath, index, overwriteExisting=False)

        for i in basediff.iter_change_type('A'):
            path = i.b_path
            blob = i.b_blob
            index.updateFile(path, blob)
            if isWikiPo(path) and getLang(path) == config.defaultlang:
                remotepot = pot(remote.tree/path)
                msg += addNonMainGitLangs(path, remotepot, repopath, index)

        for i in basediff:
            if i.change_type not in ("R", "M"):
                continue

            path = i.b_path
            if not isWikiPo(path):
                try:
                    blob = i.b_blob
                    index.updateFile(path, blob)
                except ValueError:
                    if path.startswith("submodules/"):
                        logger.error("did found a rename/modification for %s (weblate should never change submodules).", path)
                        index.removeFile(path)
                        # gitPython has a bug, that you can't use
                        # git.IndexEntry.from_blob for subproject blobs
                        # as python can't detect the size of such an blob correctly.

                        # This code is a copy of git.IndexEntry.from_blob
                        # with blob.size = 0
                        time = git.util.pack(">LL", 0, 0)
                        index.index.entries[(path, 0)] = git.IndexEntry((i.b_blob.mode, i.b_blob.binsha, 0, i.b_blob.path, time, time, 0, 0, 0, 0, 0))
                        index.index.write(ignore_extension_data=True)
                    else:
                        raise
                continue

            if getLang(path) == config.defaultlang:
                remotepot = pot(remote.tree/path)
                basepot = pot(base[0].tree/i.a_path)
                if remotepot != basepot:
                    msg += updateNonMainGitLangs(i.a_path, path, remotepot, local.commit.tree, repopath, index)

            if i.a_path not in local_modified_files:
                blob = i.b_blob
                index.updateFile(path, blob)
                continue

            # remote and local have modified the file
            m = "{path}: merging.\n".format(path=path)
            logger.debug(m.strip())
            local_modified_files.remove(i.a_path)
            with temporaryDirectory(path) as tempdir:
                temppath = pathlib.Path(tempdir)
                potpath = path
                (temppath/'pot').write_bytes(pot(remote.tree/potpath))
                msguniq(remote.tree/path, temppath/'remote')
                msguniq(local.commit.tree/i.a_path, temppath/'local')
                msguniq(base[0].tree/i.a_path, temppath/'base')

                mergePo(temppath, getLang(path), repopath/path)

            index.removeFile(path)
            index.add([path])
            msg += m

            try:
                index.removeFromUnmergedBlobMap(path)
            except KeyError:
                pass

        # if a file is only local modified, use local version, otherwise use remote's one
        for path in copy.copy(local_modified_files):
            if isWikiPo(path):
                blob = local.commit.tree/path
            else:
                # weblate is only allowed to modify po files in wiki/src.
                m = "{path}: replace with version of remote (weblate is not allowed to change this file.)\n".format(path=path)
                msg += m
                logger.warning(m.strip())
                try:
                    blob = remote.tree/path
                except KeyError:
                    index.removeFile(path)
                    continue

            index.updateFile(path, blob)

        # We need to write the index to file, before we can commit
        index.index.write()

        # Set the working tree to the new status of "local" and empty the index
        local.checkout(force=True)

        # We need to commit before we can test, if we simply can do a fast-forward
        # It seems that the diff against an index is very error-prone.
        index.index.commit(msg.strip(), parent_commits=(local.commit, remote))
    finally:
        # Get rid of temporary index file, that we don't need anymore
        if os.path.exists(index.index.path):
            logger.debug("Deleting temporary index file {index_path}.".format(index_path=index.index.path))
            os.unlink(index.index.path)

    if ff_possible_local and not local.commit.diff(remote):
        # We can fast-forward so let's do it
        local.commit = remote
        logger.info("Fast-forwarded local branch to {remote}.".format(remote=remote))
    elif local_uptodate and not local.commit.diff(oldcommit):
        # We don't have to do anything.
        local.commit = oldcommit
        logger.info("Nothing to commit - local branch can be fast-forwarded from {remote}.".format(remote=remote))

    # Set the working tree to the new status of "local" and empty the index
    local.checkout(force=True)


def commandline():
    import argparse
    import shutil

    global logger
    global prog

    debugPath = pathlib.Path("__debug")

    logging.config.fileConfig(SCRIPT_DIR + '/../config/mergeCanonicalChanges.conf')
    logger = logging.getLogger('')
    logger.level = logging.INFO

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose",
        action='store_true',
        help="verbose logging.")
    parser.add_argument(
        "-d", "--debug",
        action='store_true',
        help="keep intermediate step files in {}/.".format(debugPath))
    parser.add_argument(
        "--local",
        default="master",
        help="name of local branch.")
    parser.add_argument(
        "--localCommit",
        help="reset the branch to commit first before merging. Useful for testing.")
    parser.add_argument(
        "repopath",
        type=pathlib.Path,
        help="path to the repository.")
    parser.add_argument(
        "remote",
        help="name of remote branch or commit id.")
    args = parser.parse_args()
    prog = parser.prog

    if args.verbose:
        logger.level = logging.DEBUG

    if args.debug:
        logger.info('Store all temporary files in %s/.', debugPath)
        if debugPath.exists():
            shutil.rmtree(debugPath)

        def temporaryDirectory(path):
            temppath = pathlib.Path(path)
            if debugPath not in temppath.parents:
                temppath = debugPath/temppath
            temppath.mkdir(parents=True)
            return temppath
    else:
        def temporaryDirectory(path):
            return tempfile.TemporaryDirectory()
    try:
        main(args.repopath, args.local, args.localCommit, args.remote)
    except:
        logger.exception("-- Something unexpected happened. Giving up. --")
        raise
    else:
        logger.info("-- Ended successfully.")


if __name__ == "__main__":
    commandline()
