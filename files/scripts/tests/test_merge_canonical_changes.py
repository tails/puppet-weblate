import contextlib
import functools
import logging
import os
import pathlib
import tempfile
import unittest
from unittest.mock import patch, Mock, call

import git
import merge_canonical_changes as mcc

from .utils import createRepo

@contextlib.contextmanager
def testConfig():
    config = mcc.config
    path = pathlib.Path(os.path.realpath(__file__))
    mcc.config = mcc.Config([path.with_name("testlangs.json")])
    try:
        yield
    finally:
        mcc.config = config

def configDecorator(func):
    @functools.wraps(func)
    def f(*args, **kwargs):
        with testConfig():
            return func(*args, **kwargs)
    return f

class TestMCC(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.TemporaryDirectory()
        cls.repopath = pathlib.Path(cls.tempdir.name)
        createRepo(str(cls.repopath))
        cls.repopath = cls.repopath/"repo"
        cls.repo = git.Repo(str(cls.repopath))

    @classmethod
    def tearDownClass(cls):
        cls.tempdir.cleanup()

    def test_config_not_existent(self):
        config = mcc.Config([pathlib.Path('/not/existent')])
        with self.assertRaises(mcc.ConfigNotFoundError) as e:
            config.mainlangs
        self.assertEqual(e.exception.args, ("No config was found, searched in: %s", [pathlib.Path('/not/existent')]))

    def test_config_existent(self):
        with testConfig():
            self.assertEqual(mcc.config.mainlangs, ["de", "fr"])
            self.assertEqual(mcc.config.additionallangs, ["it", "fa"])
            self.assertEqual(mcc.config.defaultlang, "de")

    def test_isWikiPo(self):
        self.assertEqual(mcc.isWikiPo("wiki/src/adsfasd.po"), True)
        self.assertEqual(mcc.isWikiPo("wiki/src/adsfasd/asdfadsf/sdf/adsf.po"), True)
        self.assertEqual(mcc.isWikiPo("wiki/src/adsfasd"), False)
        self.assertEqual(mcc.isWikiPo("bla/wiki/src/adsfasd"), False)
        self.assertEqual(mcc.isWikiPo("bla/wiki/src/adsfasd/asdfadsf/sdf/adsf.po"), False)
        self.assertEqual(mcc.isWikiPo("bla/wiki/src/adsfasdpo"), False)
        self.assertEqual(mcc.isWikiPo(pathlib.Path("wiki/src/adsfasd.po")), True)
        self.assertEqual(mcc.isWikiPo(pathlib.Path("wiki/src/adsfasd")), False)

    def test_getLang(self):
        self.assertEqual(mcc.getLang("test.de.po"), "de")
        self.assertEqual(mcc.getLang("x/y/test.de.po"), "de")
        self.assertEqual(mcc.getLang("x/y/test.foo.fa..de.po"), "de")
        self.assertEqual(mcc.getLang("x/y/test.foo.fa..po"), "")
        with self.assertRaises(mcc.NoPropperPoFile) as e:
            mcc.getLang("x/y/test.foo.fa..de")
        self.assertEqual(e.exception.args, ("x/y/test.foo.fa..de",))
        with self.assertRaises(mcc.NoPropperPoFile) as e:
            mcc.getLang("x/y/test.po")
        self.assertEqual(e.exception.args, ("x/y/test.po",))
        with self.assertRaises(mcc.NoPropperPoFile) as e:
            mcc.getLang("x/y/test")
        self.assertEqual(e.exception.args, ("x/y/test",))

    def setGit(self, local, remote):
        self.path = pathlib.Path(os.path.realpath(__file__))
        self.local = self.repo.refs["master"]
        self.local.commit = self.repo.commit(local)
        self.local.checkout(force=True)
        self.repo.git.clean(force=True)
        if remote:
            self.remote = self.repo.commit(remote)
        else:
            self.remote = None
        self.base = self.repo.merge_base(self.local, self.remote)

        # create index to prepare the merge
        self.index = mcc.Index(git.IndexFile.from_tree(self.repo, self.base, self.local, self.remote))

    def commit(self):
        self.index.index.write()

        #commit index
        self.index.index.commit("test commit", parent_commits=(self.local.commit, self.remote))

        # chechout new created commit and clean stage
        self.local.checkout(force=True)
        self.repo.git.clean(force=True)


    @configDecorator
    def test_addNonMainGitLangs(self):
        self.setGit("master-localchanges", "remote-1")

        _p = "a.de.po"
        remotepot = mcc.pot(self.remote.tree/_p)
        msg = mcc.addNonMainGitLangs(_p, remotepot, self.repopath, self.index)

        self.assertEqual(msg.strip(), "a.it.po: adding as a.de.po has added.\na.fa.po: adding as a.de.po has added.")
        self.assertEqual((self.repopath/"a.fa.po").read_text(),
                         (self.path.with_name("data")/"a.fa.po").read_text())
        self.assertEqual((self.repopath/"a.it.po").read_text(),
                         (self.path.with_name("data")/"a.it.po").read_text())

    @configDecorator
    def test_addNonMainGitLangsOverwriteExisting(self):
        self.setGit("master-localchanges", "remote-1")

        _p = "a.de.po"
        remotepot = mcc.pot(self.remote.tree/_p)
        msg = mcc.addNonMainGitLangs(_p, remotepot, self.repopath, self.index, overwriteExisting=False)

        self.assertEqual(msg.strip(), "a.it.po: adding as a.de.po has added.")

        self.assertEqual((self.repopath/"a.fa.po").read_text(),
                         (self.path.with_name("data")/"a.fa.po.localchanges").read_text())
        self.assertEqual((self.repopath/"a.it.po").read_text(),
                         (self.path.with_name("data")/"a.it.po").read_text())

    @configDecorator
    def test_runForNonMainGit(self):
        self.setGit("master-localchanges", None)

        tree = self.local.commit.tree
        mock = Mock()
        mcc.runForNonMainGit("a.de.po", tree, mock)

        blob = tree/"a.fa.po"
        mock.assert_called_with("a.fa.po", "fa", blob)

        self.local.commit = self.repo.commit("master-added")
        tree = self.local.commit.tree
        mock = Mock()
        mcc.runForNonMainGit("a.de.po", tree, mock)

        blobFa = tree/"a.fa.po"
        blobIt = tree/"a.it.po"
        mock.assert_has_calls([call("a.fa.po", "fa", blobFa),
                               call("a.it.po", "it", blobIt)], any_order=True)

        self.local.commit = self.repo.commit("remote-addWiki")
        tree = self.local.commit.tree
        mock = Mock()
        with patch('merge_canonical_changes.getLang', return_value="xx", spec=True, autospect=True, unsafe=True):
            # Non existing file
            mcc.runForNonMainGit("wiki/src/a.de.po", tree, mock)
            mock.assert_not_called()
            mcc.runForNonMainGit("wiki/src/index.de.po", tree, mock)
            blobDe = tree/"wiki/src/index.de.po"
            blobFr = tree/"wiki/src/index.fr.po"
            mock.assert_has_calls([call("wiki/src/index.de.po", "xx", blobDe),
                                   call("wiki/src/index.fr.po", "xx", blobFr)], any_order=True)

    @configDecorator
    def test_unlinkNonMainGitLangs(self):
        self.setGit("master-added", "remote-delete")

        tree = self.local.commit.tree
        basepath = "a.de.po"
        msg = mcc.unlinkNonMainGitLangs(basepath, tree, self.index)

        self.assertEqual(msg.strip(), "a.fa.po: unlink, as a.de.po has unlinked.\na.it.po: unlink, as a.de.po has unlinked.")

        self.commit()

        self.assertEqual((self.repopath/"a.fa.po").exists(), False)
        self.assertEqual((self.repopath/"a.it.po").exists(), False)

    @configDecorator
    def test_moveNonMainGitLangs(self):
        self.setGit("master-added", "remote-rename")

        tree = self.local.commit.tree
        basepath = "a.de.po"
        msg = mcc.moveNonMainGitLangs(basepath, "b.de.po", tree, self.repopath, self.index)

        self.assertEqual(msg.strip(), "a.fa.po: moving -> b.fa.po, as a.de.po has moved.\na.it.po: moving -> b.it.po, as a.de.po has moved.")

        self.commit()

        self.assertEqual((self.repopath/"a.fa.po").exists(), False)
        self.assertEqual((self.repopath/"a.it.po").exists(), False)

        self.assertEqual((self.repopath/"b.fa.po").read_text(),
                         (self.path.with_name("data")/"a.fa.po.localchanges").read_text())
        self.assertEqual((self.repopath/"b.it.po").read_text(),
                         (self.path.with_name("data")/"a.it.po").read_text())


    @configDecorator
    def test_updateNonMainGitLangs(self):
        self.setGit("master-added", "remote-update")

        tree = self.local.commit.tree
        basepath = "a.de.po"
        _p = "a.de.po"
        remotepot = mcc.pot(self.remote.tree/_p)
        msg = mcc.updateNonMainGitLangs(basepath, _p, remotepot, tree, self.repopath, self.index)

        self.assertEqual(msg.strip(), "a.fa.po: updating as a.de.po has changed.\na.it.po: updating as a.de.po has changed.")

        self.commit()
        self.maxDiff = None

        self.assertEqual((self.repopath/"a.fa.po").read_text(),
                         (self.path.with_name("data")/"a.fa.po.update").read_text())
        self.assertEqual((self.repopath/"a.it.po").read_text(),
                         (self.path.with_name("data")/"a.it.po.update").read_text())


class TestMCCMain(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.TemporaryDirectory()
        cls.repopath = pathlib.Path(cls.tempdir.name)
        createRepo(str(cls.repopath))
        cls.repopath = cls.repopath/"repo"
        cls.repo = git.Repo(str(cls.repopath))
        cls.local = cls.repo.refs["master"]
        cls.path = pathlib.Path(os.path.realpath(__file__)).parent

    @classmethod
    def tearDownClass(cls):
        cls.tempdir.cleanup()

    def merge_message(self, lines):
        head = "merge with main git using merge_canonical_changes.\n\n"
        return (head+"\n".join(lines)).strip()

    def test_nothing_to_do(self):
        """remote and master are on the same commit"""
        mcc.logger = logging.getLogger()
        with self.assertLogs(level="DEBUG") as cm:
            mcc.main(self.repopath, "master", "remote-1", "remote-1")
        self.assertEqual(cm.output[-1], "INFO:root:remote and local are on the same commit, nothing to do.")

    def test_remote_deleted_nonWikiPo(self):
        """remote deleted a nonWikiPo file, local don't -> fast-forward."""
        mcc.main(self.repopath, "master", "remote-1", "remote-delete")
        self.assertEqual(self.local.commit, self.repo.commit("remote-delete"))

    def test_remote_add_nonWikiPo(self):
        """remote add a nonWikiPo file, local don't -> fast-forward."""
        mcc.main(self.repopath, "master", "remote-1", "remote-add")
        self.assertEqual(self.local.commit, self.repo.commit("remote-add"))

    def test_remote_change_nonWikiPo(self):
        """remote change a nonWikiPo file, local don't -> fast-forward."""
        mcc.main(self.repopath, "master", "remote-1", "remote-update")
        self.assertEqual(self.local.commit, self.repo.commit("remote-update"))

    def test_remote_change_nonWikiPo_submodule(self):
        """remote change a submodule, local don't -> fast-forward."""
        mcc.main(self.repopath, "master", "base-1", "submodule-change")
        self.assertEqual(self.local.commit, self.repo.commit("submodule-change"))

    def test_local_change_nonWikiPo_submodule(self):
        """local change a submodule, remote don't -> reset."""
        mcc.main(self.repopath, "master", "submodule-change", "base-1")
        self.assertNotEqual(self.local.commit, self.repo.commit("submodule-change"))
        self.assertNotEqual(self.local.commit, self.repo.commit("base-1"))

        submodule = self.repo.submodule('subtest')
        self.assertEqual(submodule.hexsha, submodule.module().commit("base-1").hexsha)
        self.assertEqual(self.local.commit.message, self.merge_message([
            "submodules/test: replace with version of remote (weblate is not allowed to change this file.)",
            ]))

    @configDecorator
    def test_remote_delete_wikiPo(self):
        """remote delete a wikiPo file, that still exists locally -> fast-forward."""
        mcc.main(self.repopath, "master", "remote-wiki-add", "remote-wiki-delete")
        self.assertEqual(self.local.commit, self.repo.commit("remote-wiki-delete"))

    @configDecorator
    def test_remote_delete_wikiPo_already_deleted(self):
        """remote delete a wikiPo file, that is also deleted locally -> delete this file."""
        mcc.main(self.repopath, "master", "remote-wiki-delete", "remote-wiki-delete2")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-delete"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-delete2"))

        self.assertFalse("wiki" in self.local.commit.tree)
        self.assertEqual(self.local.commit.message, self.merge_message([]))

    @configDecorator
    def test_remote_add_wikiPo(self):
        """remote adds a wikiPo file -> adds (it,fa) langs."""
        mcc.main(self.repopath, "master", "remote-1", "remote-wiki-add")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-add"))

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/index.it.po: adding as wiki/src/index.de.po has added.",
            "wiki/src/index.fa.po: adding as wiki/src/index.de.po has added.",
            ]))

        self.assertTrue("wiki/src/index.it.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/index.fa.po" in self.local.commit.tree/"wiki/src")

    @configDecorator
    def test_remote_rename_wikiPo(self):
        """remotes rename wikiPo file -> adds (it,fa) langs."""
        mcc.main(self.repopath, "master", "remote-wiki-add", "remote-wiki-rename")

        self.assertFalse("wiki/src/index.de.po" in self.local.commit.tree/"wiki/src")
        self.assertFalse("wiki/src/index.fr.po" in self.local.commit.tree/"wiki/src")
        self.assertFalse("wiki/src/index.mdwn" in self.local.commit.tree/"wiki/src")

        self.assertTrue("wiki/src/new.de.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/new.fr.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/new.mdwn" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/new.it.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/new.fa.po" in self.local.commit.tree/"wiki/src")

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/new.it.po: adding as wiki/src/new.de.po has added.",
            "wiki/src/new.fa.po: adding as wiki/src/new.de.po has added.",
            ]))
    @configDecorator
    def test_remote_change_wikiPo(self):
        """remote changes a wikiPo file -> fast-forward."""
        mcc.main(self.repopath, "master", "remote-wiki-add", "remote-wiki-update2")
        self.assertEqual(self.local.commit, self.repo.commit("remote-wiki-update2"))

    def test_only_local_has_changes(self):
        """local has commits, that are not available at remote."""
        mcc.main(self.repopath, "master", "remote-wiki-update", "remote-wiki-add")
        self.assertEqual(self.local.commit, self.repo.commit("remote-wiki-update"))

    @configDecorator
    def test_remote_change_wikiPo_local_too(self):
        """remote and local changes a wikiPo file -> merge wikiPo file."""
        mcc.main(self.repopath, "master", "remote-wiki-update", "remote-wiki-update2")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-update"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-update2"))

        self.assertEqual((self.repopath/"wiki/src/index.de.po").read_text(), (self.path/"data/index.de.po.mcc").read_text())

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/index.de.po: merging.",
            ]))


if __name__ == '__main__':
        unittest.main()
