import logging
import os
import pathlib
import unittest

import weblate_permissions as tp
from weblate.auth.models import Group, Role, User

class testTP(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.existing_roles = set(i.name for i in Role.objects.all())
        cls.existing_groups = set(i.name for i in Group.objects.all())
        cls.existing_user = set(i.username for i in User.objects.all())
        cls.datapath = pathlib.Path(os.path.realpath(__file__)).with_name("data")

    @classmethod
    def tearDownClass(cls):
        for i in Role.objects.all():
            if i.name not in cls.existing_roles:
                i.delete()
        for i in Group.objects.all():
            if i.name not in cls.existing_groups:
                i.delete()
        for i in User.objects.all():
            if i.username not in cls.existing_user:
                i.delete()

    def setUp(self):
        self.user1 = User.objects.create(username="User1")
        self.group1 = Group.objects.create(name="Group1")
        self.role1 = Role.objects.create(name="Role1")

    def tearDown(self):
        for i in Role.objects.all():
            if i.name not in self.existing_roles:
                i.delete()
        for i in Group.objects.all():
            if i.name not in self.existing_groups:
                i.delete()
        for i in User.objects.all():
            if i.username not in self.existing_user:
                i.delete()

    def test_create_roles_and_groups(self):
        group = Group.objects.filter(name="Test Group").first()
        self.assertEqual(group, None)

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:Missing role(Test Role)',
            'WARNING:weblate_permissions:Missing group(Test Group)',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' + Test Group\n'
            ' - Users\n'
            ' - Viewers',
            'WARNING:weblate_permissions:Group mismatch for user(User1)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Group\n'
            ' - Users\n'
            ' - Viewers'])
        # Make sure that group still does not exist
        group = Group.objects.filter(name="Test Group").first()
        self.assertEqual(group, None)

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, True)
        self.assertEqual(cm.output, ['INFO:weblate_permissions.audit:Create role(Test Role)',
            'INFO:weblate_permissions.audit:Create group(Test Group)',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
            'INFO:weblate_permissions.audit:Group mismatch for user(User1)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Group\n'
            ' - Users\n'
            ' - Viewers',
            'INFO:weblate_permissions.audit:Save user(User1)'])

        group = Group.objects.filter(name="Test Group").first()
        self.assertEqual(set(i.name for i in group.roles.all()), set(['Test Role',]))
        self.assertEqual(set(i.name for i in self.user1.groups.all()), set(['Test Group',]))
        self.assertEqual(group.project_selection, tp.SELECTION_ALL)
        self.assertEqual(group.language_selection, tp.SELECTION_ALL)
        role = Role.objects.filter(name="Test Role").first()
        self.assertEqual(set(i.codename for i in role.permissions.all()), set(['vcs.access', 'vcs.view', 'suggestion.add']))

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, [
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
        ])

    def test_superuser(self):
        User.objects.create(username="User2")
        e = tp.Config([self.datapath/'superuser.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:Undefined groups are used in User1\n Users\n Viewers',
            'WARNING:weblate_permissions:Undefined groups are used in User2\n Users\n Viewers',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
            'WARNING:weblate_permissions:Wrong superuser status for User1 (False != True)'])
        self.assertEqual(User.objects.filter(username="User1").first().is_superuser, False)

        e = tp.Config([self.datapath/'superuser.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, True)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:Undefined groups are used in User1\n Users\n Viewers',
            'WARNING:weblate_permissions:Undefined groups are used in User2\n Users\n Viewers',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
            'INFO:weblate_permissions.audit:Wrong superuser status for User1 (False != True)',
            'INFO:weblate_permissions.audit:Save user(User1)'])
        self.assertEqual(User.objects.filter(username="User1").first().is_superuser, True)

        e = tp.Config([self.datapath/'superuser.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:Undefined groups are used in User1\n Users\n Viewers',
            'WARNING:weblate_permissions:Undefined groups are used in User2\n Users\n Viewers',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
        ])

    def test_changes(self):
        User.objects.create(username="User2")
        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        tp.weblate_permission(e, True)
        e = tp.Config([self.datapath/'permissions_stage2.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        lines =[i.splitlines()[0] for i in cm.output[1:]]
        self.assertIn('WARNING:weblate_permissions:Permission mismatch for role(Test Role)', lines)
        self.assertIn('WARNING:weblate_permissions:Group Test Group should have a manual languages selection. (1 != 0)', lines)
        self.assertIn('WARNING:weblate_permissions:Code mismatch for group(Test Group)', lines)
        self.assertIn('WARNING:weblate_permissions:Group Test Group should have a manual project selection. (1 != 0)', lines)
        self.assertIn('WARNING:weblate_permissions:Project mismatch for group(Test Group)', lines)
        self.assertIn('WARNING:weblate_permissions:Group mismatch for user(User2)', lines)
        self.assertIn('WARNING:weblate_permissions:Wrong is_active status for User2 (True != False)', lines)
        self.assertEqual(User.objects.filter(username="User2").first().is_active, True)

        e = tp.Config([self.datapath/'permissions_stage2.yml'])
        with self.assertLogs(logger="weblate_permissions.audit", level="INFO") as cm:
            tp.weblate_permission(e, True)
        self.assertEqual(len(cm.output), len(lines)+2)  # +3 - We have touched 3 objects that needs saving
                                                        # -1 - the "More groups found, than defined" is not
                                                        #      part of the audit log
        self.assertEqual(User.objects.filter(username="User2").first().is_active, False)

        e = tp.Config([self.datapath/'permissions_stage2.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:Undefined groups are used in User2\n Users',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
        ])

    def test_deletion(self):
        User.objects.create(username="User2")
        e = tp.Config([self.datapath/'permissions_stage2.yml'])
        tp.weblate_permission(e, True)

        e = tp.Config([self.datapath/'permissions_stage3.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:role(Test Role) found, that is marked as deleted.',
            'WARNING:weblate_permissions:Additional group(Test Group) detected.',
            'WARNING:weblate_permissions:Roles for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Test Role',
            'WARNING:weblate_permissions:Codes for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - de',
            'WARNING:weblate_permissions:Projects for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Tails',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Test Group\n'
            ' - Users\n'
            ' - Viewers',
            'WARNING:weblate_permissions:Group mismatch for user(User1)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Test Group'])

        e = tp.Config([self.datapath/'permissions_stage3.yml'])
        with self.assertLogs(logger="weblate_permissions.audit", level="INFO") as cm:
            tp.weblate_permission(e, True)
        self.assertEqual(cm.output, ['INFO:weblate_permissions.audit:Delete role(Test Role) with permissions:\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - vcs.access\n'
            ' - vcs.view',
            'INFO:weblate_permissions.audit:Codes for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - de',
            'INFO:weblate_permissions.audit:Projects for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Tails',
            'INFO:weblate_permissions.audit:group(Test Group) deleted.'])


        e = tp.Config([self.datapath/'permissions_stage3.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, ['WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
        ])

    def test_same_group_name(self):
        test_group = [
            Group.objects.create(name="Test Group"),
            Group.objects.create(name="Test Group"),
        ]
        self.assertEqual([bool(i) for i in test_group], [True, True])

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, [
            'WARNING:weblate_permissions:Missing role(Test Role)',
            'WARNING:weblate_permissions:Additional group(Test Group) detected.', 
            'WARNING:weblate_permissions:Roles mismatch for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Role',
            'WARNING:weblate_permissions:Group Test Group should select every languages. (0 != 1)',
            'WARNING:weblate_permissions:Group Test Group should select every project. (0 != 1)',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
            'WARNING:weblate_permissions:Group mismatch for user(User1)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Group\n'
            ' - Users\n'
            ' - Viewers',
            ])

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, True)
        self.assertEqual(cm.output, [
            'INFO:weblate_permissions.audit:Create role(Test Role)',
            'WARNING:weblate_permissions:Additional group(Test Group) detected.',
            'INFO:weblate_permissions.audit:group(Test Group) deleted.',
            'INFO:weblate_permissions.audit:Roles mismatch for group(Test Group)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Role',
            'INFO:weblate_permissions.audit:Group Test Group should select every languages. (0 != 1)',
            'INFO:weblate_permissions.audit:Group Test Group should select every project. (0 != 1)',
            'INFO:weblate_permissions.audit:Save group(Test Group)',
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
            'INFO:weblate_permissions.audit:Group mismatch for user(User1)\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' + Test Group\n'
            ' - Users\n'
            ' - Viewers',
            'INFO:weblate_permissions.audit:Save user(User1)'
        ])

        group = Group.objects.filter(name="Test Group").all()
        self.assertEqual(len(group), 1)

        e = tp.Config([self.datapath/'permissions_stage1.yml'])
        with self.assertLogs(level="DEBUG") as cm:
            tp.weblate_permission(e, False)
        self.assertEqual(cm.output, [
            'WARNING:weblate_permissions:More groups found, than defined.\n'
            ' +++ expected\n'
            ' --- actual\n'
            ' - Administration\n'
            ' - Group1\n'
            ' - Guests\n'
            ' - Managers\n'
            ' - Project creators\n'
            ' - Reviewers\n'
            ' - Users\n'
            ' - Viewers',
        ])
