import contextlib
import functools
import os
import pathlib
import sys
import tempfile
import unittest
from unittest.mock import patch, Mock, call

import git

import tailsWeblate
import update_weblate_components as uwc
uwc.BASEPATH="."

from .utils import createRepo

@contextlib.contextmanager
def testConfig():
    config = uwc.mcc.config
    path = pathlib.Path(os.path.realpath(__file__))
    uwc.mcc.config = uwc.mcc.Config([path.with_name("testlangs.json")])
    try:
        yield
    finally:
        uwc.mcc.config = config

def configDecorator(func):
    @functools.wraps(func)
    def f(*args, **kwargs):
        with testConfig():
            return func(*args, **kwargs)
    return f

class TestUWC(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.tempdir_orig = tempfile.TemporaryDirectory()
        createRepo(cls.tempdir_orig.name)
        cls.repo_orig = git.Repo(os.path.join(cls.tempdir_orig.name, "repo"))
        cls.tempdir_clone = tempfile.TemporaryDirectory()
        cls.repopath = pathlib.Path(cls.tempdir_clone.name)
        cls.repo = cls.repo_orig.clone(str(cls.repopath))

    @classmethod
    def tearDownClass(cls):
        cls.tempdir_orig.cleanup()
        cls.tempdir_clone.cleanup()

    def setUp(self):
        self.path = pathlib.Path(os.path.realpath(__file__))
        self.subproject = tailsWeblate.models.Component.objects.first()

    def setGit(self, local):
        self.local = self.repo.refs["master"]
        self.local.commit = self.repo.commit(local)
        self.local.checkout(force=True)
        self.repo.git.clean(force=True)

    @configDecorator
    def test_deleteComponent(self):
        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.deleteComponent("a.de.po")
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:deleted file (ignoring): a.de.po"])

        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.deleteComponent("wiki/src/a.fr.po")
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:deleted file (ignoring, wrong lang): wiki/src/a.fr.po"])
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            # Mock would trigger self.project's __getattr__() if we didn't prevent it here.
            with patch('weblate.utils.stats.BaseStats._is_coroutine', new=lambda: None, create=True):
                subproject = Mock(spec=self.subproject)
                with patch('tailsWeblate.subProject', return_value=subproject):
                    uwc.deleteComponent("wiki/src/index.de.po")
                    subproject.delete.assert_called_once_with()
                # Test object is gone now
        self.assertEqual(cm.output, ["INFO:update_weblate_components:deleted component: wiki/src/index.de.po"])

    @configDecorator
    @patch('tailsWeblate.renameComponent', autospec=True)
    def test_renameComponent(self, renameComponent):
        self.setGit("remote-addWiki")

        master = self.local
        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.renameComponent("b.mdwn", "a.mdwn", master)
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:renamed file (ignoring): b.mdwn -> a.mdwn"])

        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.renameComponent("wiki/src/b.fr.po", "wiki/src/a.fr.po", master)
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:renamed file (ignoring, wrong lang): wiki/src/b.fr.po -> wiki/src/a.fr.po"])

        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            uwc.renameComponent("wiki/src/old.de.po", "wiki/src/index.de.po", master)
        self.assertEqual(cm.output, ["INFO:update_weblate_components:renamed component: wiki/src/old.de.po -> wiki/src/index.de.po"])
        renameComponent.assert_called_once_with("wiki/src/old.de.po", "wiki/src/index.de.po")

        renameComponent.reset_mock()
        renameComponent.side_effect = tailsWeblate.TailsWeblateException("an exception")
        with self.assertLogs("update_weblate_components", level="WARNING") as cm:
            uwc.renameComponent("wiki/src/old.de.po", "wiki/src/index.de.po", master)
        self.assertEqual(cm.output, ["WARNING:update_weblate_components:\t... an exception, skipping."])

        # TODO: replace with renameComponent.assert_called(), when Python >= 3.6 is default
        if renameComponent.call_count == 0:
            msg = ("Expected 'renameComponent' to have been called.")
            raise AssertionError(msg)

    @configDecorator
    @patch('tailsWeblate.addComponent', autospec=True)
    def test_addComponent(self, addComponent):
        self.setGit("remote-addWiki")

        master = self.local
        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.addComponent("a.mdwn", master)
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:added file (ignoring): a.mdwn"])

        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.addComponent("wiki/src/a.fr.po", master)
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:added file (ignoring, wrong lang): wiki/src/a.fr.po"])

        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            uwc.addComponent("wiki/src/index.de.po", master)
        self.assertEqual(cm.output, ["INFO:update_weblate_components:added component: wiki/src/index.de.po"])
        addComponent.assert_called_once_with("wiki/src/index.de.po")

        addComponent.reset_mock()
        addComponent.side_effect = tailsWeblate.TailsWeblateException("an exception")
        with self.assertLogs("update_weblate_components", level="WARNING") as cm:
            uwc.addComponent("wiki/src/index.de.po", master)
        self.assertEqual(cm.output, ["WARNING:update_weblate_components:\t... an exception, skipping."])

        # TODO: replace with addComponent.assert_called(), when Python >= 3.6 is default
        if addComponent.call_count == 0:
            msg = ("Expected 'addComponent' to have been called.")
            raise AssertionError(msg)

    @configDecorator
    @patch('tailsWeblate.subProject', autospec=True)
    def test_updateComponent(self, subProject):
        self.setGit("remote-addWiki")

        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.updateComponent("a.mdwn")
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:modified file (ignoring): a.mdwn"])

        with self.assertLogs("update_weblate_components", level="DEBUG") as cm:
            uwc.updateComponent("wiki/src/index.mdwn")
        self.assertEqual(cm.output, ["DEBUG:update_weblate_components:modified file (ignoring): wiki/src/index.mdwn"])

        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            uwc.updateComponent("wiki/src/index.de.po")
        self.assertEqual(cm.output, ["INFO:update_weblate_components:modified file: wiki/src/index.de.po"])
        subProject.assert_called_once_with("wiki/src/index.de.po")

    @configDecorator
    def test_perform_update(self):
        self.setGit("remote-1")

        oldHead = self.local.commit
        master = self.local

        newHead = self.repo.commit('remote-add')
        with patch('update_weblate_components.addComponent', autospec= True) as addComponent:
            uwc.perform_update(oldHead, newHead, master)
        addComponent.assert_has_calls([call("b.de.po", master),
                                       call("b.mdwn", master)], any_order=True)

        with patch('update_weblate_components.addComponent', autospec= True, side_effect=Exception("exeption")):
            with self.assertLogs("update_weblate_components", level="ERROR") as cm:
                self.assertRaises(Exception, uwc.perform_update, oldHead, newHead, master)
        self.assertTrue(cm.output[0].startswith("ERROR:update_weblate_components:Adding new component triggered by 'b.de.po' failed:\nTraceback"))

        newHead = self.repo.commit('remote-delete')
        with patch('update_weblate_components.deleteComponent', autospec= True) as deleteComponent:
            uwc.perform_update(oldHead, newHead, master)
        deleteComponent.assert_has_calls([call("a.de.po"),
                                          call("a.fr.po"),
                                          call("a.mdwn")], any_order=True)

        with patch('update_weblate_components.deleteComponent', autospec= True, side_effect=Exception("exeption")):
            with self.assertLogs("update_weblate_components", level="ERROR") as cm:
                self.assertRaises(Exception, uwc.perform_update, oldHead, newHead, master)
        self.assertTrue(cm.output[0].startswith("ERROR:update_weblate_components:Deletion triggered by 'a.de.po' failed:\nTraceback"))

        newHead = self.repo.commit('remote-rename')
        with patch('update_weblate_components.renameComponent', autospec= True) as renameComponent:
            uwc.perform_update(oldHead, newHead, master)
        renameComponent.assert_has_calls([call("a.de.po", 'b.de.po', master),
                                          call("a.fr.po", 'b.fr.po', master),
                                          call("a.mdwn", 'b.mdwn', master)], any_order=True)

        with patch('update_weblate_components.renameComponent', autospec= True, side_effect=Exception("exeption")):
            with self.assertLogs("update_weblate_components", level="ERROR") as cm:
                self.assertRaises(Exception, uwc.perform_update, oldHead, newHead, master)
        self.assertTrue(cm.output[0].startswith("ERROR:update_weblate_components:Rename triggered by 'a.de.po'->'b.de.po' failed:\nTraceback"))

        newHead = self.repo.commit('remote-update')
        with patch('update_weblate_components.updateComponent', autospec=True) as updateComponent:
            # Mock would trigger self.project's __getattr__() if we didn't prevent it here.
            with patch('weblate.utils.stats.BaseStats._is_coroutine', new=lambda: None, create=True):
                mock = Mock(spec=self.subproject)
                updateComponent.return_value=mock
                uwc.perform_update(oldHead, newHead, master)
                mock.create_translations.assert_called_once_with(force=False, langs=["de"])
        updateComponent.assert_called_once_with("a.de.po")

        newHead = self.repo.commit('remote-update')
        with patch('update_weblate_components.updateComponent', autospec=True) as updateComponent:
            updateComponent.return_value=None
            uwc.perform_update(oldHead, newHead, master)
        updateComponent.assert_called_once_with("a.de.po")

        with patch('update_weblate_components.updateComponent', autospec= True, side_effect=Exception("exeption")):
            with self.assertLogs("update_weblate_components", level="ERROR") as cm:
                self.assertRaises(Exception, uwc.perform_update, oldHead, newHead, master)
        self.assertTrue(cm.output[0].startswith("ERROR:update_weblate_components:Updating component triggered by 'a.de.po' failed:\nTraceback"))

    @patch("update_weblate_components.perform_update", autospec=True)
    def test_mainFetch(self, mock_perform_update):
        self.setGit("remote-1")

        oldHead = self.local.commit
        newHead = self.repo.commit('remote-add')

        master = self.repo.refs['origin/master']

        orig_master = self.repo_orig.refs['master']
        orig_master.commit = self.repo_orig.commit('remote-delete')

        orig_remote = self.repo_orig.refs['remote']
        orig_remote.commit = self.repo_orig.commit('remote-1')

        # Master branch hasn't changed and remote is up-to-date -> do nothing
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            with patch.object(sys, 'argv', ['update_weblate_component.py', str(self.repopath)]):
                uwc.main()
                self.assertFalse(mock_perform_update.called)
            self.assertEqual(cm.output, ["INFO:update_weblate_components:-- Head not changed - nothing to do. --"])

        # remote is up-to-date -> do nothing
        self.repo.remotes['origin'].fetch('master')
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            with patch.object(sys, 'argv', ['update_weblate_component.py', "--fetch", str(self.repopath)]):
                uwc.main()
            self.assertEqual(cm.output[-1], "INFO:update_weblate_components:-- Head not changed - nothing to do. --")
            self.assertFalse(mock_perform_update.called)

        # origin/remote is up-to-date -> do nothing
        orig_remote.commit = self.repo_orig.commit('remote-add')
        self.repo.remotes['origin'].fetch()
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            with patch.object(sys, 'argv', ['update_weblate_component.py', "--remoteBranch", 'origin/remote', "--fetch", str(self.repopath)]):
                uwc.main()
            self.assertEqual(cm.output[-1], "INFO:update_weblate_components:-- Head not changed - nothing to do. --")
            self.assertFalse(mock_perform_update.called)

        # updated remote branch from remote-1 -> remote-add
        # -> will trigger update of master branch to remote-add
        master.commit = self.repo_orig.commit('remote-1')
        orig_master.commit = self.repo_orig.commit('remote-add')
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            with patch.object(sys, 'argv', ['update_weblate_component.py', "--fetch", str(self.repopath)]):
                uwc.main()
                mock_perform_update.assert_called_once_with(oldHead, newHead, master)
                mock_perform_update.reset_mock()
                self.assertEqual(self.repo.refs['master'].commit, self.repo.commit('remote-add'))
                self.assertEqual(cm.output[-1], "INFO:update_weblate_components:-- Finished successfully. --")

        # remote updates in a non-forward manner
        master.commit = self.repo_orig.commit('remote-1')
        orig_master.commit = self.repo_orig.commit('remote-delete')
        with self.assertLogs("update_weblate_components", level="INFO") as cm:
            with patch.object(sys, 'argv', ['update_weblate_component.py', "--fetch", str(self.repopath)]):
                self.assertRaises(git.exc.GitCommandError, uwc.main)
                self.assertEqual(cm.output[-1], "ERROR:update_weblate_components:Couldn't update master with fast-forward strategy. Reset remote to {} again".format(self.repo.commit('remote-1')))
                self.assertEqual(self.repo.refs['master'].commit, self.repo.commit('remote-add'))


    @patch("update_weblate_components.perform_update", autospec=True)
    def test_mainStartEnd(self, mock_perform_update):
        self.setGit("remote-1")

        oldHead = self.local.commit
        master = self.repo.refs['origin/master']

        newHead = self.repo.commit('remote-add')
        baseHead = self.repo.commit('base-1')

        with patch.object(sys, 'argv', ['update_weblate_component.py', '--end', 'remote-add', str(self.repopath)]):
            uwc.main()
            mock_perform_update.assert_called_once_with(oldHead, newHead, master)
            mock_perform_update.reset_mock()

        with patch.object(sys, 'argv', ['update_weblate_component.py', '--end', newHead.hexsha , str(self.repopath)]):
            uwc.main()
            mock_perform_update.assert_called_once_with(oldHead, newHead, master)
            mock_perform_update.reset_mock()

        with patch.object(sys, 'argv', ['update_weblate_component.py', '--start', 'base-1', str(self.repopath)]):
            uwc.main()
            mock_perform_update.assert_called_once_with(baseHead, oldHead, master)
            mock_perform_update.reset_mock()

        with patch.object(sys, 'argv', ['update_weblate_component.py', '--start', baseHead.hexsha , str(self.repopath)]):
            uwc.main()
            mock_perform_update.assert_called_once_with(baseHead, oldHead, master)
            mock_perform_update.reset_mock()


if __name__ == '__main__':
        unittest.main()
