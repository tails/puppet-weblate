#!/bin/sh
#
# This script will integrate changes from Weblate and main Tails repositories
# by doing the following:
#
#   - Merge changes from main repo into integration repo
#   - Merge changes from Weblate repo into integration repo
#   - Push integrated changes to the main repo (via gatekeeper)
#   - Update lists of components in Weblate
#
#
# Important: in order to push integrated changes to the main repo, the user
# runinng this script must have a SSH key configured in our Gitolite's
# configuration for the `weblate-gatekeeper.git` repository. This is currently
# not handled by Puppet and needs to be setup manually.

set -e
set -u

PIDFILE=/tmp/weblate-cronjob.pid

INTEGRATION_GIT_CHECKOUT=/integration
WEBLATE_GIT_CHECKOUT=/app/data/vcs/tails/index
UPDATE_LOGFILE=/var/log/weblate/update.log
SCRIPTS_DIR=/scripts

log() {
   echo "$(date '+%F %T') - cron.sh - ${*}" >> "$UPDATE_LOGFILE"
}

git_fetch() {
  checkout="$1"
  remote="$2"

  log "Updating '${remote}' remote of in '${checkout}'..."
  (
    cd "$checkout"
    git fetch "$remote" >> "$UPDATE_LOGFILE" 2>&1
    log "done."
  )
}

git_submodule_update() {
  checkout="$1"

  log "Updating submodules of '${checkout}'..."
  (
    cd "$checkout"
    git submodule update -f --init >> "$UPDATE_LOGFILE" 2>&1
    log "done."
  )
}

git_push() {
   checkout="$1"
   remote="$2"
   branch="$3"

  log "Pushing ${branch} branch of '${checkout}' to the '${remote}' remote..."
  (
    cd "${checkout}"
    git push --quiet ${remote} ${branch} >> "$UPDATE_LOGFILE" 2>&1
    log "done."
  )
}


docron() {
  log "Merging changes from main repo into the integration repo..."
  git_fetch "${INTEGRATION_GIT_CHECKOUT}" origin
  ${SCRIPTS_DIR}/merge_canonical_changes.py \
    "${INTEGRATION_GIT_CHECKOUT}" origin/master
  log "done."

  git_submodule_update ${INTEGRATION_GIT_CHECKOUT}

  git_push "${INTEGRATION_GIT_CHECKOUT}" weblate-gatekeeper master

  log "Fetching and merging changes made by Weblate into the integration repo..."
  git_fetch "${INTEGRATION_GIT_CHECKOUT}" weblate
  ${SCRIPTS_DIR}/merge_weblate_changes.py \
    "${INTEGRATION_GIT_CHECKOUT}" weblate/master
  log "done."

  git_push "${INTEGRATION_GIT_CHECKOUT}" weblate-gatekeeper master

  git_fetch "${WEBLATE_GIT_CHECKOUT}" origin

  log "Updating Weblate components in Weblate repo..."
  ${SCRIPTS_DIR}/update_weblate_components.py \
    --remoteBranch=cron/master \
    --fetch \
    "${WEBLATE_GIT_CHECKOUT}"
  log "done."

  log "Updating Core Pages component list..."
  ${SCRIPTS_DIR}/update_core_pages.py
  log "done."
}

if ! pgrep --pidfile="$PIDFILE" >/dev/null 2>&1; then
  echo $$ > "$PIDFILE"
  log "Starting docron run..."
  docron
  log "Completed docron run."
  rm "$PIDFILE"
fi
