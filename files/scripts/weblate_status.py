#!/usr/bin/env python3

"""Script to detect issues with Weblate components vs git repository.
It can happen that the Weblate components get out of sync for the git repostory.
This detects/fixes following issues:
* Remove additional languages files from git, if those were forgotten to delete ({DEFAULTLANG}.po dos not exist).
* Add additional languages files in git, if the {DEFAULTLANG}.po file exists. This also helps if you updated the list of supported languages.
* Scan through the list of Weblate components and add missing components and delete leftovers.
"""

import argparse
import atexit
from collections import defaultdict
import itertools
import git
import logging
import os
import pathlib
import sys

import tailsWeblate
import merge_canonical_changes as mcc
PATH = pathlib.Path("/app/data/vcs/tails/index")

repo = git.Repo(str(PATH))
index = mcc.Index(repo.index)
tree = repo.tree()

LANGS = mcc.config.mainlangs + mcc.config.additionallangs
LANGS.sort()

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)
logger = logging.getLogger('')
logging.getLogger('git.cmd').level = logging.INFO
logger.level = logging.INFO

parser = argparse.ArgumentParser()
parser.add_argument(
    "-v", "--verbose",
    action='store_true',
    help="verbose logging.")
parser.add_argument(
    "--modify",
    action='store_true',
    help="really make changes to git/weblate otherwise just prints out things, that should be fixed.")
args = parser.parse_args()
prog = parser.prog

if args.modify:
    # Make sure, that no cron process it triggered while this script is running
    pidfile = '/tmp/weblate-cronjob.pid'
    try:
        with open(pidfile) as f:
            pid = int(f.read())
        if os.kill(pid, 0):
            sys.exit("An other cron job is running ({pid}). Please retry later.".format(pid=pid))
    except FileNotFoundError:
        pass
    except ProcessLookupError:
        pass
    with open(pidfile, 'w') as f:
            f.write(str(os.getpid())+"\n")

    @atexit.register
    def removePidfile():
        try:
            with open(pidfile) as f:
                pid = int(f.read())
            if pid == os.getpid():
                os.unlink(pidfile)
            else:
                logger.error("Content of {pidfile}: %s and this is not us %s -> keep pidfile", pidfile, pid, os.getpid())
        except FileNotFoundError:
            pass

if args.verbose:
    logger.level = logging.DEBUG

# Get all components that are currently in git
fileComponents = defaultdict(list)
for f in (PATH / 'wiki/src').rglob('*.po'):
    *name, lang, _ = f.name.split(".")
    base = f.with_name(".".join(name))
    fileComponents[base.relative_to(PATH)].append(lang)

# We sort the lang list, for checking for equality
for c in fileComponents.values():
    c.sort()


def testlang(item):
    return item[1] == LANGS


uncompleteComponents = list(itertools.filterfalse(testlang, fileComponents.items()))

msg = "update weblate git using {}.\n\n".format(sys.argv[0])

for path, langs in uncompleteComponents:
    defaultLangPath = path.with_name("{}.{}.po".format(path.name, mcc.config.defaultlang))
    if mcc.config.defaultlang in langs:
        logger.info('missing language files for {}'.format(path))
        potFile = mcc.pot(tree/str(defaultLangPath))
        if args.modify:
            msg += mcc.addNonMainGitLangs(defaultLangPath, potFile, PATH, index, overwriteExisting=False)
    else:
        logger.info('language file leftovers for {}.'.format(path))
        if args.modify:
            try:
                tree/str(path.parent)
            except KeyError:
                pass
            else:
                msg += mcc.unlinkNonMainGitLangs(defaultLangPath, tree, index)
            for lang in langs:
                (PATH/path.with_name("{}.{}.po".format(path.name, lang))).unlink()
        del(fileComponents[path])

# There were changes done in git - commit those changes.
if uncompleteComponents:
    index.index.write()
    index.index.commit(msg.strip())
    repo.refs['master'].checkout(force=True)

# Get all components defined in Weblate
weblateComponents = {}
for sp in tailsWeblate.models.Component.objects.all():
    componentPath = pathlib.Path(sp.filemask[:-5])
    if componentPath == pathlib.PosixPath('.'):
        continue
    weblateComponents[componentPath] = sp

for c in set(fileComponents) - set(weblateComponents):
    logger.info('missing component: {}'.format(c))
    if args.modify:
        tailsWeblate.addComponent(str(c) + ".*.po")

for c in set(weblateComponents) - set(fileComponents):
    logger.info('deleting leftover component: {}'.format(c))
    if args.modify:
        weblateComponents[c].delete()
