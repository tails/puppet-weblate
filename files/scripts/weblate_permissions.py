#!/usr/bin/env python3

'''Check and enforce permissions for Weblate.

By default, this script checks Weblate permissions against the ones described
in a config file. Use `--enforce` to actually apply the permissions from the
config file to Weblate.


Objects that are checked/modified
---------------------------------

Only the following Weblate objects are checked and possibly modified by this
script:

    - All groups and roles defined in the config file.
    - All users with permissions defined in the config file.
    - All users that are not in the config file but are active in Weblate.

We deliberately avoid going through inactive users because those include
"deleted-XXX" users, and that is because deactivating+renaming is the way
Weblate handles user deletion. Add an inactive user to the config file to force
it to be checked.

The script will warn you and (if `--enforce` is passed) will create the roles
and groups that are defined but do not yet exist in Weblate. Removing a group
or role from the config file will not delete it but will warn you if that
object is used anywhere in the config. Add a `__deleted` flag to a group or
role to actually delete it.

Weblate always updates it owns groups and roles, so to keep our setup secure,
we should not try to delete those roles/groups and use our own copies, see
tails#20132. Here is the list of Weblate default groups and roles:

https://docs.weblate.org/en/latest/admin/access.html#default-teams-and-roles

(Weblate now uses the term "team" instead of "group")


Configuration file
------------------

The script searches for a config file named `weblate_permission.yaml` in `.`,
`..` or `../config`.

The YAML config file has four section: `groups`, `roles`, `user` and
`superusers`.

In the examples below, the Capitalized strings are the ones you should change
according to your needs. Strings in lowercase should not be changed as they are
words reserved by the script.

For codes, projects you can either define a list of strings, or use the magic entry:

- __all  # All projects/languages
- __all_portected  # All protected projects/languages
- __all_public  # All public projects/languages

    group:
      Group 1:
        __deleted: False   # Delete the group? (default: False)
        codes:             # List of languages codes. Set to a list with a
          - de             #   single item with value `__all` to select all
          - ...            #   languages.

        projects:          # List of projects to set for this group. Set to a
          - Project 1      #   list with a single item with value `__all` to
          - ...            #   select all projects.

        roles:             # List of roles to set for this group.
          - Role 1
          - ...

        users:             # List of users to add to this group.
          - User 1
      Group 2:
        ...

    roles:
      Role 1:              # List of roles. Set to a list iwth a single item
        - suggestion.add   #   `__deleted` to delete this role.
        - ...
      Role 2:
        ...

    users:
      User 1:
        is_active: True    # Is the user active? (default: True)
        groups:            # List of group names. Add an item with value
          - Group 1        #   `__default` to add this user to all groups
      User 2:              #   defined in the `__default` user template
        ...                #   (see below for more info).

    superusers:
      - User1              # List of users that should be superusers.


The `__default` user template
-----------------------------

For all users that are not defined in the config file, the `__default` user
template is applied. You can define such template in the top level and/or in
the `users` section of each group.

When no `__default` user template is defined, the following defaults are used
as a fallback:

    - is_active: True
    - groups: []

Every user that is active in Weblate but not defined in the config file will be
checked and (if `--enforce`) added to all the groups defined for the
`__default` user template. To override this behavior you need to explicitly add
the user to the `users` section.


.maintenance file
-----------------

If a `.maintenance` files lives next to the config file, the script falls back
to "check mode". This should give service maintainers the freedom to do
maintenance without the script interfering in the work. To run the script in
"enforce mode" even when the `.maintenance` file exists, you have to pass the
`--enforceMaintenance` option.
'''

import logging
import logging.config
import operator
import yaml
import tailsWeblate    # Needed because of the side effect to setup the django application
from weblate.auth.models import Group, Language, Permission, Project, Role, User

from weblate.auth.data import (
    SELECTION_ALL,
    SELECTION_ALL_PROTECTED,
    SELECTION_ALL_PUBLIC,
    SELECTION_COMPONENT_LIST,
    SELECTION_MANUAL,
)

SELECTION = {"__all": SELECTION_ALL,
             "__all_protected": SELECTION_ALL_PROTECTED,
             "__all_public": SELECTION_ALL_PUBLIC,
            }

class ConfigNotFoundError(Exception):
    pass


class Config:
    def __init__(self, paths):
        self.paths = paths
        self.config = None

    def load(self):
        for path in self.paths:
            if path.exists():
                with path.open() as f:
                    self.config = yaml.safe_load(f)
                self.path = path
                break
        else:
            self.config = None
            raise ConfigNotFoundError("No config was found, searched in: %s", self.paths)

    def __getattr__(self, item):
        if not self.config:
            self.load()
        return self.config.get(item)


PERMISSIONS_NAME = "weblate_permissions.yaml"

logger = logging.getLogger(__name__)
auditlogger = logger.getChild('audit')


def format_diff(actual, expected):
    ''' return a diff kind of output of actual and expect.'''
    ret = ["+++ expected", "--- actual"]
    for i in sorted(actual | expected):
        if i not in actual:
            ret.append(f"+ {i}")
        if i not in expected:
            ret.append(f"- {i}")
    return ret


def check(actual, expected, logline, logfunc, diff=True):
    ''' check if actual == expected, if not print logline.
        - diff=True will print a diff like output about the differences in lists/sets'''
    if actual == expected:
        return True
    if diff:
        logfunc(logline+"\n "+"\n ".join(format_diff(actual, expected)))
    else:
        logfunc(logline+f" ({actual} != {expected})")
    return False


def element_check(element, attr, expected, logline, logfunc, diff=True):
    return check(getattr(element, attr), expected, logline, logfunc, diff)


def check_and_enforce(element, attr, expected, logline, logfunc, diff=True):
    if not element_check(element, attr, expected, logline, logfunc, diff):
        setattr(element, attr, expected)
        return False
    return True


class DBQuery:
    def __init__(self, query, attr, pool):
        self.query = query
        self.attr = attr
        self.pool = pool

    def check(self, expected, logline, logfunc, diff=True):
        func = operator.attrgetter(self.attr)
        actual = set(func(i) for i in self.query.all())
        return check(actual, expected, logline, logfunc, diff)

    def enforce(self, expected):
        self.query.set(self.pool.filter(**{self.attr+"__in":expected}), clear=True)

    def check_and_enforce(self, expected, logline, logfunc, diff=True):
        if not self.check(expected, logline, logfunc, diff):
            self.enforce(expected)
            return False
        return True


class WeblatePermission:

    def __init__(self, config, enforce):
        self.config = config
        self.enforce = enforce

        self._prepare_config()

    def _prepare_config(self):

        # replace lists with sets, as order is not relevant in this context
        for k,v in self.config.roles.items():
            self.config.roles[k] = set(v)

        self.roles = set(k for k,v in self.config.roles.items() if not '__deleted' in v)
        self.groups = set(k for k,v in self.config.groups.items() if not v.get('__deleted'))

        if self.config.users is None:
            self.config.config['users'] = dict()

        if self.config.superusers is None:
            self.config.config['superusers'] = list()

        if not '__default' in self.config.users:
            self.config.users['__default']={'is_active': True, 'groups':set()}

        for v in self.config.users.values():
            v["groups"] = set(v.get("groups",[]))

        # replace lists with sets, as the ordering does not contain information
        # add all implicit users to __default groups (not defined under users)
        for g,v in sorted(self.config.groups.items(), key=lambda i: i[0]):
            if v is None:
                v = {}
                self.config.groups[g] = v
            v['roles'] = set(v.get('roles', []))
            if v['roles'] - self.roles:
                logger.warning('Undefined roles are used in {}\n {}'.format(g,'\n '.join(sorted(v['roles'] - self.roles))))
            v['codes'] = set(v.get('codes', []))
            v['projects'] = set(v.get('projects', []))
            if v.get('__deleted'):
                continue
            for user in v.get('users', []):
                if user not in self.config.users:
                    self.config.users[user] = {'is_active': True, 'groups': set(('__default',))}
                self.config.users[user]['groups'].add(g)

        # replace lists with sets, as the ordering does not contain information
        # replace __default group with all default groups
        for u,v in sorted(self.config.users.items(), key=lambda i: i[0]):
            if u == '__default':
                continue
            if '__default' in v["groups"]:
                v["groups"].remove('__default')
                v["groups"] |= self.config.users['__default']['groups']
            if v['groups'] - self.groups:
                logger.warning('Undefined groups are used in {}\n {}'.format(u,'\n '.join(sorted(v['groups'] - self.groups))))

    def weblate_permission(self):

        self.attribute = operator.attrgetter("check")
        self.check_func = element_check
        self.logfunc = logger.warning
        if self.enforce:
            self.attribute = operator.attrgetter("check_and_enforce")
            self.check_func = check_and_enforce
            self.logfunc = auditlogger.info

        self.fix_roles()
        self.fix_groups()
        self.fix_users()

    def fix_roles(self):
        for name, e in sorted(self.config.roles.items(), key=operator.itemgetter(0)):
            i = Role.objects.filter(name=name).first()
            if i:
                dbquery = DBQuery(i.permissions, 'codename', Permission.objects)
                if '__deleted' in e:
                    if self.enforce:
                        dbquery.check(set(), f'Delete role({name}) with permissions:', self.logfunc)
                        i.delete()
                    else:
                        logger.warning(f'role({name}) found, that is marked as deleted.')
                    continue

                if not self.attribute(dbquery)(e, f"Permission mismatch for role({name})", self.logfunc):
                    if self.enforce:
                        auditlogger.info(f"Save role({name})")
                        i.save()
            else:
                if '__deleted' in e:
                    continue
                if self.enforce:
                    auditlogger.info(f"Create role({name})")
                    role = Role.objects.create(name=name)
                    role.permissions.set(Permission.objects.filter(codename__in=e), clear=True)
                    role.save()
                else:
                    logger.warning(f"Missing role({name})")

    def _del_group(self, group):
        name = group.name
        dbquery = DBQuery(group.roles, 'name', Role.objects)
        dbquery.check(set(), f"Roles for group({name})", self.logfunc)
        if group.language_selection == SELECTION_MANUAL:
            dbquery = DBQuery(group.languages, 'code', Language.objects)
            dbquery.check(set(), f"Codes for group({name})", self.logfunc)
        if group.project_selection == SELECTION_MANUAL:
            dbquery = DBQuery(group.projects, 'name', Project.objects)
            dbquery.check(set(), f"Projects for group({name})", self.logfunc)
        if self.enforce:
            group.delete()
            auditlogger.info(f"group({name}) deleted.")

    def fix_groups(self):
        for name, e in sorted(self.config.groups.items(), key=operator.itemgetter(0)):

            # As Weblate allows multiple groups with the same name and we need to
            # handle all, so we use the all method.
            g = Group.objects.filter(name=name).all()

            # The downsidoe of the all method is, that we cannot simply decide, if
            # there is no group with that name, so we end up with a try/execpt
            # block.

            # `i` is the "main group" that we will changed acording the config file
            # and all other groups with the same name will be deleted.
            try:
                i = g[0]
            except IndexError:
                i = None

            def _fe_set(prop:str):
                "'first element' of a set, if there is only one element."
                prop = e[prop]
                if len(prop) != 1:
                    return None

                return next(iter(prop))

            if i:
                changed = False

                if e.get('__deleted'):
                    if self.enforce:
                        logger.warning(f"Delete group({name}).")
                    else:
                        logger.warning(f"Additional group({name}) detected.")
                    self._del_group(i)
                    continue

                # Delete additional groups with same name.
                # The current version of Weblate (5.3.1) has no uniqueness for
                # group names. We endend up with tails#20132, because there were
                # two groups with the same name and different permissions/roles.
                for g in g[1:]:
                    logger.warning(f"Additional group({name}) detected.")
                    self._del_group(g)

                dbquery = DBQuery(i.roles, 'name', Role.objects)
                if not self.attribute(dbquery)(e['roles'], f"Roles mismatch for group({name})", self.logfunc):
                    changed = True

                if _fe_set('codes') in SELECTION:
                    if not self.check_func(i, 'language_selection', SELECTION[_fe_set('codes')], f"Group {name} should select every languages.", self.logfunc, False):
                        changed = True
                else:
                    if not self.check_func(i, 'language_selection', SELECTION_MANUAL, f"Group {name} should have a manual languages selection.", self.logfunc, False):
                        changed = True

                    dbquery = DBQuery(i.languages, 'code', Language.objects)
                    if not self.attribute(dbquery)(e['codes'], f"Code mismatch for group({name})", self.logfunc):
                        changed = True

                if _fe_set('projects') in SELECTION:
                    if not self.check_func(i, 'project_selection', SELECTION[_fe_set('projects')], f"Group {name} should select every project.", self.logfunc, False):
                        changed = True
                else:
                    if not self.check_func(i, 'project_selection', SELECTION_MANUAL, f"Group {name} should have a manual project selection.", self.logfunc, False):
                        changed = True
                    dbquery = DBQuery(i.projects, 'name', Project.objects)
                    if not self.attribute(dbquery)(e['projects'], f"Project mismatch for group({name})", self.logfunc):
                        changed = True
                if changed and self.enforce:
                    auditlogger.info(f"Save group({name})")
                    i.save()
            else:
                if e.get('__deleted'):
                    continue
                if self.enforce:
                    auditlogger.info(f"Create group({name})")
                    args = {'name':name,
                            'project_selection': SELECTION_ALL,
                            'language_selection': SELECTION_ALL,
                    }
                    group = Group.objects.create(**args)
                    group.roles.set(Role.objects.filter(name__in=e['roles']), clear=True)
                    if _fe_set('projects') in SELECTION:
                        group.project_selection = SELECTION[_fe_set('projects')]
                    else:
                        group.project_selection = SELECTION_MANUAL
                        group.projects.set(Project.objects.filter(name__in=e['projects']), clear=True)

                    if _fe_set('codes') in SELECTION:
                        group.language_selection = SELECTION[_fe_set('codes')]
                    else:
                        group.language_selection = SELECTION_MANUAL
                        group.languages.set(Language.objects.filter(code__in=e['codes']), clear=True)
                    group.save()

                else:
                    logger.warning(f"Missing group({name})")

        # Check if no additional groups are defined.
        func = operator.attrgetter("name")
        a = set(func(i) for i in Group.objects.all())
        check(a, self.groups, "More groups found, than defined.", logger.warning)

    def fix_users(self):
        for i in sorted(User.objects.all(),key=lambda i:i.username.lower()):
            changed = False

            if not i.is_active and i.username not in self.config.users:
                # Ignore inactive users, if they are not listed explictitly
                continue

            e = self.config.users.get(i.username, self.config.users['__default'])

            dbquery = DBQuery(i.groups, 'name', Group.objects)
            if not self.attribute(dbquery)(e['groups'], f"Group mismatch for user({i.username})", self.logfunc):
                changed = True
            if not self.check_func(i, 'is_superuser', i.username in self.config.superusers, f"Wrong superuser status for {i.username}", self.logfunc, False):
                changed = True
            if not self.check_func(i,'is_active', e.get('is_active'), f"Wrong is_active status for {i.username}", self.logfunc, False):
                changed = True
            if changed and self.enforce:
                auditlogger.info(f"Save user({i.username})")
                i.save()


def weblate_permission(expected, enforce):
    w_p = WeblatePermission(expected, enforce)
    w_p.weblate_permission()


def commandline():
    import argparse
    import os
    import pathlib

    global logger
    global auditlogger

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose",
        action='store_true',
        help="verbose logging.")
    parser.add_argument(
        "--enforce",
        action='store_true',
        help="enforce permissions.")
    parser.add_argument(
        "--enforceMaintenance",
        action='store_true',
        help="enforce permissions even in maintanance mode.")
    args = parser.parse_args()

    path = pathlib.Path(os.path.realpath(__file__))
    config = Config([pathlib.Path('.'+PERMISSIONS_NAME), path.with_name(PERMISSIONS_NAME), path.parent.with_name("config")/PERMISSIONS_NAME])
    config.load()

    logging.config.fileConfig([str(config.path.with_name('weblate_permissions.conf'))])
    logger = logging.getLogger('')
    logger.level = logging.INFO
    auditlogger = logger.getChild('audit')

    if args.verbose:
        logger.level = logging.DEBUG

    if args.enforceMaintenance:
        args.enforce = True

    if args.enforce:
        if not args.enforceMaintenance and config.path.with_name('.maintenance').exists():
            args.enforce = False
            logger.warning('System is in maintance mode - not enforcing permissions, just run check mode.\nUse --enforceMaintanance to overwrite.')
    if args.enforce:
        logger.info('Run in enforce mode.')
    else:
        logger.info('Run in check mode.')
    try:
        weblate_permission(config, args.enforce)
    except:
        logger.exception('Failed with exception.')
        raise
    logger.info('Finish run.')

if __name__ == "__main__":
    commandline()
