# Default interval for Weblate writing pending commits to Git.
COMMIT_PENDING_HOURS = 1

COMPRESS_ENABLED = True

# For reasoning behind SSL settings, see:
# https://gitlab.tails.boum.org/tails/sysadmin/-/issues/17339

# This is handy for dev and doesn't affect production because our frontend
# proxy sets 'X-Forwarded-Proto: https', which tells Django that the request is
# already secure.
SECURE_SSL_REDIRECT = False

# Number of nearby messages to show in each direction
NEARBY_MESSAGES = 105
