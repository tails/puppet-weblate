require 'spec_helper'

describe 'weblate::podman' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  let(:params) do
    {
      postgres_password: '123',
      redis_password: '456',
      weblate_admin_password: '789',
      weblate_secret_key: '012',
      weblate_email_host_user: 'someuser',
      weblate_email_host_password: 'mystrongpass'
    }
  end

  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_class('podman').with(
          manage_subuid: true,
          podman_docker_pkg_ensure: 'absent',
        )
      end

      it do
        is_expected.to contain_podman__subuid('weblate').with(
          subuid: 2000001,
          count: '65536',
        )
      end

      it do
        is_expected.to contain_podman__subgid('weblate').with(
          subgid: 2000001,
          count: '65536',
        )
      end

      it do
        is_expected.to contain_sysctl__value('kernel.unprivileged_userns_clone').with(
          value: '1',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/data/secret').with(
          ensure: 'file',
          owner: 2001000,
          group: 2000000,
          content: '012',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/podman.env').with(
          ensure: 'file',
          owner: 2000000,
          group: 2000000,
          mode: '0640',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/data/settings-override.py').with(
          ensure: 'file',
          source: 'puppet:///modules/weblate/config/settings-override.py',
          owner: 2001000,
          group: 2000000,
          mode: '0644',
        )
      end

      it do
        is_expected.to contain_podman__container('weblate').with(
          image: 'docker.io/weblate/weblate:5.3.0.1',
          user: 'weblate',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/run_in_container.sh').with(
          ensure: 'file',
          owner: 2000000,
          group: 2000000,
          mode: '0755',
        )
      end

      it do
        is_expected.to contain_file('/usr/local/bin/podman-weblate').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0755',
          source: 'puppet:///modules/weblate/podman-weblate.py',
        )
      end
    end
  end
end
