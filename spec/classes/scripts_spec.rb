require 'spec_helper'

describe 'weblate::scripts' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts').with(
          ensure: 'directory',
          owner: 2000000,
          group: 2000000,
          mode: '0755',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/update_core_pages.py').with(
          source: 'puppet:///modules/weblate/scripts/update_core_pages.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/updateCorePages.conf').with(
          source: 'puppet:///modules/weblate/config/updateCorePages.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/save-suggestions.py').with(
          source: 'puppet:///modules/weblate/scripts/save-suggestions.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/saveSuggestions.conf').with(
          source: 'puppet:///modules/weblate/config/saveSuggestions.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/update_weblate_components.py').with(
          source: 'puppet:///modules/weblate/scripts/update_weblate_components.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/updateWeblateComponents.conf').with(
          source: 'puppet:///modules/weblate/config/updateWeblateComponents.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/merge_canonical_changes.py').with(
          source: 'puppet:///modules/weblate/scripts/merge_canonical_changes.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/mergeCanonicalChanges.conf').with(
          source: 'puppet:///modules/weblate/config/mergeCanonicalChanges.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/langs.json').with(
          content: "{\n  \"mainlangs\": [ \"\" ],\n  \"defaultlang\": \"de\",\n  \"additionallangs\": [ \"\" ]\n}\n",
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/merge_weblate_changes.py').with(
          source: 'puppet:///modules/weblate/scripts/merge_weblate_changes.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/mergeWeblateChanges.conf').with(
          source: 'puppet:///modules/weblate/config/mergeWeblateChanges.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/weblate_status.py').with(
          source: 'puppet:///modules/weblate/scripts/weblate_status.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/tailsWeblate.py').with(
          source: 'puppet:///modules/weblate/scripts/tailsWeblate.py',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/weblate_permissions.py').with(
          source: 'puppet:///modules/weblate/scripts/weblate_permissions.py',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/weblate_permissions.conf').with(
          source: 'puppet:///modules/weblate/config/weblate_permissions.conf',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/weblate_permissions.yaml').with(
          source: 'puppet:///modules/weblate/config/weblate_permissions.yaml',
          mode: '0644',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_cron('Check & enforce Weblate permissions model').with(
          command: '/var/lib/weblate/scripts/run_in_container.sh /scripts/weblate_permissions.py --enforce',
          user: 'weblate',
          hour: '5',
          minute: '21',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/update-staging-website.py').with(
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_cron('weblate update staging').with(
          command: '/var/lib/weblate/scripts/update-staging-website.py',
          user: 'weblate',
          hour: '3',
          minute: '21',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/cron.sh').with(
          source: 'puppet:///modules/weblate/scripts/cron.sh',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_cron('weblate cronjobs').with(
          command: 'cd /var/lib/weblate/scripts && [ -f /var/lib/weblate/config/.maintenance ] || ( /var/lib/weblate/scripts/create-snapshot.sh && /var/lib/weblate/scripts/run_in_container.sh /scripts/cron.sh )',
          user: 'weblate',
          minute: '15',
        )
      end
    end
  end
end
