require 'spec_helper'

describe 'weblate::webserver' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_service('apache2').with(
          ensure: 'running',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2dismod rewrite').with(
          onlyif: '/usr/bin/test -f /etc/apache2/mods-enabled/rewrite.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod proxy').with(
          creates: '/etc/apache2/mods-enabled/proxy.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod headers').with(
          creates: '/etc/apache2/mods-enabled/headers.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_exec('/usr/sbin/a2enmod proxy_http').with(
          creates: '/etc/apache2/mods-enabled/proxy_http.load',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/var/www/weblate').with(
          ensure: 'directory',
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/www/weblate/robots.txt').with(
          ensure: 'file',
          owner: 2000000,
          group: 2000000,
          mode: '0644',
          content: "User-agent: *\nDisallow: /translate\n",
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config/apache-vhost.conf').with(
          ensure: 'file',
          owner: 'root',
          group: 2000000,
          mode: '0664',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/apache2/sites-available/000-default.conf').with(
          ensure: 'symlink',
          target: '/var/lib/weblate/config/apache-vhost.conf',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/modsecurity/modsecurity.conf').with(
          ensure: 'file',
          source: 'puppet:///modules/weblate/modsecurity/modsecurity.conf',
          owner: 'root',
          group: 'root',
          mode: '0644',
          notify: 'Service[apache2]',
        )
      end

      it do
        is_expected.to contain_file('/etc/modsecurity/crs/crs-setup.conf').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0644',
          notify: 'Service[apache2]',
        )
      end
    end
  end
end
