require 'spec_helper'

describe 'weblate::config' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_service('memcached').with(
          ensure: 'running',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/config').with(
          ensure: 'directory',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/data').with(
          ensure: 'directory',
          owner: 2001000,
          group: 2000000,
          mode: '2755',
        )
      end
    end
  end
end
