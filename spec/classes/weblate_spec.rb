require 'spec_helper'

describe 'weblate' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_class('weblate::database').with(
          postgres_password: '123',
        )
      end

      it do
        is_expected.to contain_class('weblate::redis').with(
          redis_password: '456',
        )
      end

      it do
        is_expected.to contain_class('weblate::podman').with(
          redis_password: '456',
          postgres_password: '123',
          weblate_admin_password: '789',
          weblate_secret_key: '012',
          extra_volumes: [],
          weblate_email_host_user: 'someuser',
          weblate_email_host_password: 'mystrongpass',
        )
      end
    end
  end
end
