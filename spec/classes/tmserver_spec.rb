require 'spec_helper'

describe 'weblate::tmserver' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      # bind_address: "127.0.0.1",
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_file('/var/lib/tmserver').with(
          ensure: 'directory',
          owner: 2000000,
          group: 'tmserver',
          mode: '0775',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/tmserver/db').with(
          ensure: 'file',
          owner: 2000000,
          group: 'tmserver',
          mode: '0664',
        )
      end

      it do
        is_expected.to contain_user('tmserver').with(
          ensure: 'present',
          system: true,
          home: '/var/lib/tmserver',
          gid: 'tmserver',
        )
      end

      it do
        is_expected.to contain_group('tmserver').with(
          ensure: 'present',
          system: true,
        )
      end

      it do
        is_expected.to contain_file('/etc/systemd/system/tmserver.service').with(
          owner: 'root',
          group: 'root',
          mode: '0644',
          content: "[Unit]\nDescription=TM Server Instance\n\n[Service]\nUser=tmserver\nGroup=tmserver\nExecStart=/usr/bin/tmserver -d /var/lib/tmserver/db -b 127.0.0.1 -p 8888\n\n[Install]\nWantedBy=multi-user.target\n",
        )
      end

      it do
        is_expected.to contain_service('tmserver').with(
          ensure: 'running',
          provider: 'systemd',
          subscribe: 'File[/etc/systemd/system/tmserver.service]',
          enable: true,
        )
      end

      it do
        is_expected.to contain_file('/etc/backup.d/20.sh').with(
          ensure: 'file',
          owner: 'root',
          group: 'root',
          mode: '0400',
          content: '/usr/bin/sqlite3 /var/lib/tmserver/db ".backup \'/var/backups/tmserver.db\'"',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/scripts/update_tm.sh').with(
          content: ["#!/bin/sh\n \nset -eu\n \nUPDATE_LANGS=\"\"\n \nupdate_memory () {\n    lang=\"$1\"\n    find /var/lib/weblate/data/vcs/tails/index/wiki/src/ -name *.${lang}.po -type f | while read po_file; do\n        /usr/bin/build_tmdb -d /var/lib/tmserver/db -s en -t \"$lang\" \"$po_file\" | ( grep --quiet --invert-match \"^File added:\" || true )\n    done\n}\n \nfor lang in $UPDATE_LANGS ; do update_memory \"$lang\" ; done\n"],
          mode: '0755',
          owner: 2000000,
          group: 2000000,
        )
      end

      it do
        is_expected.to contain_cron('weblate update tmserver').with(
          command: '/var/lib/weblate/scripts/update_tm.sh',
          user: 'weblate',
          monthday: '1',
          hour: '5',
          minute: '23',
        )
      end
    end
  end
end
