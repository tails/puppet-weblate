require 'spec_helper'

describe 'weblate::logs' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_file('/var/log/weblate').with(
          ensure: 'directory',
          owner: 'root',
          group: 2000000,
          mode: '2770',
        )
      end

      it do
        is_expected.to contain_file('/var/lib/weblate/logs').with(
          ensure: 'link',
          target: '/var/log/weblate',
        )
      end

      it do
        is_expected.to contain_file('/etc/logrotate.d/weblate').with(
          source: 'puppet:///modules/weblate/logrotate/logrotate.conf',
          owner: 'root',
          group: 'root',
          mode: '0644',
        )
      end
    end
  end
end
