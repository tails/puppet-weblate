require 'spec_helper'

describe 'weblate::database' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      postgres_password: '123'
    }
  end

  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_class('postgresql::server').with(
          listen_addresses: '127.0.0.1',
        )
      end

      it do
        is_expected.to contain_postgresql__server__db('weblate').with(
          user: 'weblate',
          password: 'md5d7c196f7c73edbaf2568f6ca0404dea2',
        )
      end

      it do
        is_expected.to contain_postgresql__server__pg_hba_rule('allow Weblate container to access Weblate database').with(
          description: 'Open up PostgreSQL for access from 127.0.0.1',
          type: 'host',
          database: 'weblate',
          user: 'weblate',
          address: '127.0.0.1/32',
          auth_method: 'md5',
        )
      end

      it do
        is_expected.to contain_postgresql__server__extension('pg_trgm').with(
          database: 'weblate',
        )
      end

      it do
        is_expected.to contain_file('/etc/pgbouncer/pgbouncer.ini').with(
          ensure: 'file',
          owner: 'postgres',
          group: 'postgres',
          mode: '0640',
        )
      end

      it do
        is_expected.to contain_file('/etc/pgbouncer/userlist.txt').with(
          ensure: 'file',
          owner: 'postgres',
          group: 'postgres',
          mode: '0640',
          content: '"weblate" "md5d7c196f7c73edbaf2568f6ca0404dea2"',
        )
      end

      it do
        is_expected.to contain_service('pgbouncer').with(
          ensure: 'running',
          subscribe: ['File[/etc/pgbouncer/pgbouncer.ini]', 'File[/etc/pgbouncer/userlist.txt]'],
        )
      end
    end
  end
end
