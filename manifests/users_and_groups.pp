# @summary
#   Users and groups needed by many Weblate resources
#
# @example
#   include weblate::users_and_groups
class weblate::users_and_groups inherits weblate::params {
  ensure_packages(['grep'])

  # Enforce group GID
  group { $system_group:
    ensure => present,
    system => true,
    gid    => $system_gid,
  }

  # Enforce user UID and GID
  user { $system_user:
    ensure     => present,
    system     => true,
    home       => $weblate_home,
    uid        => $system_uid,
    gid        => $system_gid,
    groups     => ['systemd-journal'],
    managehome => true,
    require    => Group[$system_user],
  }

  file { $weblate_home:
    ensure  => directory,
    owner   => $system_user,
    group   => $system_group,
    mode    => '0750',
    require => [
      User[$system_user],
      Group[$system_group],
    ],
  }

  $ssh_dir = "${weblate_home}/.ssh"

  file { $ssh_dir:
    ensure  => directory,
    owner   => $ns_weblate_uid,
    group   => $ns_root_gid,
    mode    => '0750',
    require => [
      File[$weblate_home],
      User[$system_user],
      Group[$system_group],
    ],
  }

  exec { 'Ensure SSH directory\'s  UID/GID for containerized Weblate':
    command => "/bin/chown -R ${ns_weblate_uid}:${ns_root_gid} ${ssh_dir}",
    onlyif  => "/usr/bin/find ${ssh_dir} -not \\( -uid ${ns_weblate_uid} -a -gid ${ns_root_gid} \\) | /bin/grep .",
    require => [
      File[$ssh_dir],
      Package['grep'],
    ],
  }
}
