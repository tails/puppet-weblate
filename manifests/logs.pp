# @summary
#   Manage log directories and rotation
#
# @example
#   include weblate::logs
class weblate::logs inherits weblate::params {
  file { $system_logs_dir:
    ensure => directory,
    owner  => root,
    group  => $system_gid,
    mode   => '2770',
  }

  file { $weblate_logs_dir:
    ensure => link,
    target => $system_logs_dir,
  }

  file { '/etc/logrotate.d/weblate':
    source  => 'puppet:///modules/weblate/logrotate/logrotate.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$system_logs_dir],
  }
}
