# @summary
#   Scripts for integration with Tails website.
#
# @example
#   include weblate::scripts
class weblate::scripts inherits weblate::params {
  file { $weblate_scripts_dir:
    ensure => directory,
    owner  => $system_uid,
    group  => $system_gid,
    mode   => '0755',
  }

  # script for upgrading the core pages component list in Weblate

  file { "${weblate_scripts_dir}/update_core_pages.py":
    source  => 'puppet:///modules/weblate/scripts/update_core_pages.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File[
      "${weblate_config_dir}/updateCorePages.conf",
      "${weblate_scripts_dir}/tailsWeblate.py"
    ],
  }

  file { "${weblate_config_dir}/updateCorePages.conf":
    source => 'puppet:///modules/weblate/config/updateCorePages.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # scripts for creating temporary git repository with all suggestions for staging wiki

  file { "${weblate_scripts_dir}/save-suggestions.py":
    source  => 'puppet:///modules/weblate/scripts/save-suggestions.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File["${weblate_config_dir}/saveSuggestions.conf"],
  }

  file { "${weblate_config_dir}/saveSuggestions.conf":
    source => 'puppet:///modules/weblate/config/saveSuggestions.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # script for upgrading git repository in weblate

  file { "${weblate_scripts_dir}/update_weblate_components.py":
    source  => 'puppet:///modules/weblate/scripts/update_weblate_components.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File[
      "${weblate_config_dir}/updateWeblateComponents.conf",
      "${weblate_scripts_dir}/tailsWeblate.py",
      "${weblate_scripts_dir}/merge_canonical_changes.py"
    ],
  }

  file { "${weblate_config_dir}/updateWeblateComponents.conf":
    source => 'puppet:///modules/weblate/config/updateWeblateComponents.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # script for updating the weblate git repository from main git

  file { "${weblate_scripts_dir}/merge_canonical_changes.py":
    source  => 'puppet:///modules/weblate/scripts/merge_canonical_changes.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File["${weblate_config_dir}/mergeCanonicalChanges.conf"],
  }

  file { "${weblate_config_dir}/mergeCanonicalChanges.conf":
    source => 'puppet:///modules/weblate/config/mergeCanonicalChanges.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  file { "${weblate_config_dir}/langs.json":
    content => epp('weblate/langs.json.epp', {
        production_slave_languages   => $production_slave_languages,
        weblate_additional_languages => $weblate_additional_languages,
    }),
    mode    => '0644',
    owner   => $system_uid,
    group   => $system_gid,
  }

  # script for updating the weblate git repository from main git

  file { "${weblate_scripts_dir}/merge_weblate_changes.py":
    source  => 'puppet:///modules/weblate/scripts/merge_weblate_changes.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File[
      "${weblate_config_dir}/mergeWeblateChanges.conf",
      "${weblate_scripts_dir}/merge_canonical_changes.py"
    ],
  }

  file { "${weblate_config_dir}/mergeWeblateChanges.conf":
    source => 'puppet:///modules/weblate/config/mergeWeblateChanges.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # script to check/update Weblate components against git.

  file { "${weblate_scripts_dir}/weblate_status.py":
    source => 'puppet:///modules/weblate/scripts/weblate_status.py',
    mode   => '0755',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # commonly used functions to handle Weblate components.

  file { "${weblate_scripts_dir}/tailsWeblate.py":
    source => 'puppet:///modules/weblate/scripts/tailsWeblate.py',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # script to check Weblate role permissions.

  file { "${weblate_scripts_dir}/weblate_permissions.py":
    source  => 'puppet:///modules/weblate/scripts/weblate_permissions.py',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => File[
      "${weblate_config_dir}/weblate_permissions.conf",
      "${weblate_config_dir}/weblate_permissions.yaml",
      "${weblate_scripts_dir}/tailsWeblate.py"
    ],
  }

  file { "${weblate_config_dir}/weblate_permissions.conf":
    source => 'puppet:///modules/weblate/config/weblate_permissions.conf',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  file { "${weblate_config_dir}/weblate_permissions.yaml":
    source => 'puppet:///modules/weblate/config/weblate_permissions.yaml',
    mode   => '0644',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # Checking and enforcing models must be run inside the container
  cron { 'Check & enforce Weblate permissions model':
    command     => "${weblate_scripts_dir}/run_in_container.sh /scripts/weblate_permissions.py --enforce",
    environment => ["MAILTO=${weblate_admin_email}"],
    user        => weblate,
    hour        => 5,
    minute      => 21,
    require     => File[
      "${weblate_scripts_dir}/weblate_permissions.py"
    ],
  }

  # Cronjobs

  file { "${weblate_scripts_dir}/update-staging-website.py":
    content => epp('weblate/update-staging-website.py.epp', {
        weblate_home    => $weblate_home,
        staging_www_dir => $staging_www_dir,
        system_logs_dir => $system_logs_dir,
    }),
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
  }

  cron { 'weblate update staging':
    command     => "${weblate_scripts_dir}/update-staging-website.py",
    environment => ["MAILTO=${weblate_admin_email}"],
    user        => weblate,
    hour        => 3,
    minute      => 21,
    require     => File["${weblate_scripts_dir}/update-staging-website.py"],
  }

  file { "${weblate_home}/borg":
    ensure => directory,
    owner  => weblate,
    group  => weblate,
    mode   => '0755',
  }

  ensure_packages(['borgbackup'])

  exec { '/usr/bin/borg init -e repokey':
    creates     => "${weblate_home}/borg/data",
    user        => weblate,
    environment => [
      'BORG_PASSPHRASE=""',
      "BORG_REPO=${weblate_home}/borg",
    ],
    require     => [
      Package['borgbackup'],
      File["${weblate_home}/borg"],
    ]
  }

  file { "${weblate_scripts_dir}/create-snapshot.sh":
    content => epp('weblate/create-snapshot.sh.epp', {
      weblate_home => $weblate_home,
      keep_within  => $snapshots_keep_within,
    }),
    mode    => '0755',
    owner   => weblate,
    group   => weblate,
    require => [
      Vcsrepo[
        "${weblate_data_dir}/vcs/tails/index",
        "${weblate_repos_dir}/integration",
      ],
      Package['borgbackup'],
    ],
  }

  ensure_packages(['bash'])

  include weblate::repositories
  include weblate::logs

  file { "${weblate_scripts_dir}/cron.sh":
    source  => 'puppet:///modules/weblate/scripts/cron.sh',
    mode    => '0755',
    owner   => $system_uid,
    group   => $system_gid,
    require => [
      Vcsrepo[
        "${weblate_data_dir}/vcs/tails/index",
        "${weblate_repos_dir}/integration",
      ],
      File[
        $weblate_logs_dir,
        "${weblate_scripts_dir}/merge_canonical_changes.py",
        "${weblate_scripts_dir}/update_weblate_components.py",
        "${weblate_scripts_dir}/merge_weblate_changes.py",
      ],
      Package['bash'],
    ],
  }

  # The Weblate update job must be run inside the container
  cron { 'weblate cronjobs':
    command     => "cd ${weblate_scripts_dir} && [ -f ${weblate_config_dir}/.maintenance ] || ( ${weblate_scripts_dir}/create-snapshot.sh && ${weblate_scripts_dir}/run_in_container.sh /scripts/cron.sh )",  # lint:ignore:140chars -- command
    environment => ["MAILTO=${weblate_admin_email}"],
    user        => weblate,
    minute      => '15',
    require     => File["${weblate_scripts_dir}/cron.sh"],
  }
}
