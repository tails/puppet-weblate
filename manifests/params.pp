# @summary
#   Parameters that are common to many weblate-related classes.
#
# @example
#   include weblate::params
#
# @param production_slave_languages
#   The languages that are activated in production.
#
# @param weblate_additional_languages
#   The additional languages that are activated only in Weblate.
#
# @param system_user
#   The system user to run Weblate as.
#
# @param system_group
#   The system group to run Weblate as.
#
# @param system_uid
#   The host system UID of the Weblate user in the host system.
#
# @param system_gid
#   The host system GID of the Weblate user in the host system.
#
# @param ns_weblate_uid
#   The host system UID of the container's Weblate user.
#
# @param ns_root_gid
#   The host system GID of the container's Weblate group.
#
# @param system_logs_dir
#   The Weblate logs directory in the host system.
#
# @param weblate_home
#   The home of the Weblate user.
#
# @param weblate_scripts_dir
#   The Weblate custom scripts directory.
#
# @param weblate_config_dir
#   The Weblate config directory.
#
# @param weblate_data_dir
#   The Weblate data directory.
#
# @param weblate_repos_dir
#   The Weblate repositories directory.
#
# @param weblate_logs_dir
#   The Weblate logs directory in the Weblate's home tree.
#
# @param weblate_www_dir
#   The Weblate website root directory.
#
# @param staging_www_dir
#   The staging website root directory.
#
# @param tmserver_data_dir
#   The Translation Memory Server data directory.
#
# @param postgres_user
#   The PostgreSQL username.
#
# @param postgres_database
#   The PostgreSQL database name.
#
# @param weblate_version
#   The Weblate version to run.
#
# @param weblate_site_domain
#   The Weblate website domain.
#
# @param staging_site_domain
#   The staging website domain.
#
# @param snapshots_keep_within
#   How long to keep backup snapshots of repositories.
#
# @param weblate_admin_email
#   The admin's email address
#
class weblate::params (
  Hash $production_slave_languages,
  Hash $weblate_additional_languages,
  String $system_user                       = 'weblate',
  String $system_group                      = 'weblate',
  Integer $system_uid                       = 2000000,
  Integer $system_gid                       = 2000000,
  Integer $ns_weblate_uid                   = $system_uid + 1000,  # Namespace UID/GID to use inside the Weblate container,
  Integer $ns_root_gid                      = $system_gid,         # see `weblate::podman` for more info about this.
  Stdlib::Absolutepath $system_logs_dir     = '/var/log/weblate',
  Stdlib::Absolutepath $weblate_home        = '/var/lib/weblate',
  Stdlib::Absolutepath $weblate_scripts_dir = "${weblate_home}/scripts",
  Stdlib::Absolutepath $weblate_config_dir  = "${weblate_home}/config",
  Stdlib::Absolutepath $weblate_data_dir    = "${weblate_home}/data",
  Stdlib::Absolutepath $weblate_repos_dir   = "${weblate_home}/repositories",
  Stdlib::Absolutepath $weblate_logs_dir    = "${weblate_home}/logs",
  Stdlib::Absolutepath $weblate_www_dir     = '/var/www/weblate',
  Stdlib::Absolutepath $staging_www_dir     = '/var/www/staging',
  Stdlib::Absolutepath $tmserver_data_dir   = '/var/lib/tmserver',
  String $postgres_user                     = 'weblate',
  String $postgres_database                 = 'weblate',
  String $weblate_version                   = '5.3.0.1',
  Stdlib::Fqdn $weblate_site_domain         = 'translate.tails.net',
  Stdlib::Fqdn $staging_site_domain         = 'staging.tails.net',
  String $snapshots_keep_within             = '7d',
  String $weblate_admin_email               = 'weblate@tails.net',
) {
  $weblate_slave_languages = deep_merge($production_slave_languages, $weblate_additional_languages)
}
