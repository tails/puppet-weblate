# @summary
#   Configure the database for the translation platform.
#
# @example
#   class { 'weblate:database':
#     postgres_password => 'secret3',
#   }
#
# @param postgres_password
#   The PostgreSQL password.
#
# @param max_client_conn
#   The maximum number of PostgreSQL client connections at once.
#
# @param bind_address
#   Which address should the database service bind to.
class weblate::database (
  String $postgres_password,
  Integer[1] $max_client_conn = 200,
  String $bind_address = '127.0.0.1',
) inherits weblate::params {
  ### PostgreSQL database configuration

  class { 'postgresql::server':
    listen_addresses => $bind_address,
  }

  postgresql::server::db { $postgres_database:
    user     => $postgres_user,
    password => postgresql::postgresql_password($postgres_user, $postgres_password),
  }

  postgresql::server::pg_hba_rule { 'allow Weblate container to access Weblate database':
    description => "Open up PostgreSQL for access from ${bind_address}",
    type        => 'host',
    database    => $postgres_database,
    user        => $postgres_user,
    address     => "${bind_address}/32",
    auth_method => 'md5',
  }

  postgresql::server::extension { 'pg_trgm':
    database => $postgres_database,
  }

  ### PgBouncer -- a PostgreSQL pooling mechanism

  ensure_packages(['pgbouncer'])

  file { '/etc/pgbouncer/pgbouncer.ini':
    ensure  => file,
    owner   => postgres,
    group   => postgres,
    mode    => '0640',
    require => Package['pgbouncer'],
    content => epp('weblate/pgbouncer.ini.epp', {
        postgres_password => $postgres_password,
        max_client_conn   => $max_client_conn,
    }),
  }

  $md5_password_hash = md5("${postgres_password}${postgres_user}")

  file { '/etc/pgbouncer/userlist.txt':
    ensure  => file,
    owner   => postgres,
    group   => postgres,
    mode    => '0640',
    require => Package['pgbouncer'],
    content => "\"${postgres_user}\" \"md5${md5_password_hash}\"",
  }

  service { 'pgbouncer':
    ensure    => running,
    subscribe => [
      File['/etc/pgbouncer/pgbouncer.ini'],
      File['/etc/pgbouncer/userlist.txt'],
    ],
  }
}
