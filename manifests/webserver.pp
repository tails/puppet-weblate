# @summary
#   Setup a webserver to serve the translation platform.
#
# @example
#   include weblate::webserver
class weblate::webserver (
  Enum['On', 'Off', 'DetectionOnly'] $sec_rule_engine = 'On',
  Enum['deny,status:403', 'pass'] $sec_default_action = 'deny,status:403',
  Array $remove_rules = [],
) inherits weblate::params {
  ensure_packages([
      'apache2',
      'modsecurity-crs',
      'libapache2-mod-security2',
  ])

  service { 'apache2':
    ensure  => running,
    require => Package['apache2'],
  }

  # FIXME: remove once deployed
  exec { '/usr/sbin/a2dismod rewrite':
    onlyif  => '/usr/bin/test -f /etc/apache2/mods-enabled/rewrite.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2enmod proxy':
    creates => '/etc/apache2/mods-enabled/proxy.load',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  exec { '/usr/sbin/a2enmod headers':
    creates => '/etc/apache2/mods-enabled/headers.load',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  exec { '/usr/sbin/a2enmod proxy_http':
    creates => '/etc/apache2/mods-enabled/proxy_http.load',
    notify  => Service['apache2'],
    require => [
      Package['apache2'],
      Exec['/usr/sbin/a2enmod proxy'],
    ],
  }

  file { $weblate_www_dir:
    ensure => directory,
    mode   => '0755',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # By blocking crawlers from accesing `/translate`, we avoid them from wasting
  # resources, triggering errors in Weblate and possibly indexing unreviewed
  # content.
  file { "${weblate_www_dir}/robots.txt":
    ensure  => file,
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0644',
    content => "User-agent: *\nDisallow: /translate\n",
  }

  include weblate::config

  file { "${weblate_config_dir}/apache-vhost.conf":
    ensure  => file,
    content => epp('weblate/apache-vhost.conf.epp', {
        staging_www_dir     => $staging_www_dir,
        weblate_www_dir     => $weblate_www_dir,
        weblate_site_domain => $weblate_site_domain,
        sec_rule_engine     => $sec_rule_engine,
        remove_rules        => $remove_rules,
    }),
    owner   => root,
    group   => $system_gid,
    mode    => '0664',
    notify  => Service['apache2'],
    require => [
      File[$weblate_config_dir],
      File[$weblate_www_dir],
    ],
  }

  file { '/etc/apache2/sites-available/000-default.conf':
    ensure  => symlink,
    target  => "${weblate_config_dir}/apache-vhost.conf",
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  file { '/etc/modsecurity/modsecurity.conf':
    ensure  => file,
    source  => 'puppet:///modules/weblate/modsecurity/modsecurity.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['modsecurity-crs'],
    notify  => Service['apache2'],
  }

  file { '/etc/modsecurity/crs/crs-setup.conf':
    ensure  => file,
    content => epp('weblate/modsecurity/crs-setup.conf.epp', {
        sec_default_action => $sec_default_action,
    }),
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['modsecurity-crs'],
    notify  => Service['apache2'],
  }
}
