# @summary
#   Weblate Podman container.
#
# @example
#   class { 'weblate::podman':
#     weblate_admin_password      => 'secret1',
#     weblate_secret_key          => 'secret2',
#     postgres_password           => 'secret3',
#     redis_password              => 'secret4',
#     weblate_email_host_user     => 'username',
#     weblate_email_host_password => 'secret5',
#  }
#
# @param weblate_admin_password
#   The Weblate admin password.
#
# @param weblate_secret_key
#   The Weblate secret key.
#
# @param postgres_password
#   The PostgreSQL password.
#
# @param redis_password
#   The Redis password.
#
# @param weblate_alternative_domains
#   An Array with Weblate alternative domains.
#
# @param weblate_admin_name
#   The Weblate admin user name.
#
# @param weblate_auto_update
#   Whether to update all repositories on a daily basis.
#
# @param weblate_email_host
#   The host for sending e-mail.
#
# @param weblate_email_port
#   The port for sending e-mail.
#
# @param weblate_email_host_user
#   The user for sending e-mail.
#
# @param weblate_email_host_password
#   The password for sending e-mail.
#
# @param weblate_email_use_tls
#   Whether to use STARTTLS for sending e-mail.
#
# @param weblate_email_use_ssl
#   Whether to use SSL for sending e-mail.
#
# @param weblate_server_email
#   The email address that error messages are sent from.
#
# @param weblate_default_from_email
#   Configures the address for outgoing e-mails.
#
# @param weblate_default_commiter_email
#   Committer e-mail address defaulting to noreply@weblate.org.
#
# @param weblate_default_commiter_name
#   Committer name defaulting to Weblate.
#
# @param weblate_mt_tmserver
#   The tmserver URL.
#
# @param weblate_remove_addons
#   List of add-ons to be removed from default Weblate config.
#
# @param weblate_remove_checks
#   List of checks to be removed from default Weblate config.
#
# @param postgres_host
#   The PostgreSQL host.
#
# @param postgres_user
#   The PostgreSQL username.
#
# @param postgres_port
#   The PostgreSQL port.
#
# @param redis_host
#   The Redis host.
#
# @param redis_port
#   The Redis port.
#
# @param redis_db
#   The Redis database name.
#
# @param extra_volumes
#   Extra volumes to be mounted in the Weblate container.
class weblate::podman (
  String $weblate_admin_password,
  String $weblate_secret_key,
  String $postgres_password,
  String $redis_password,
  String $weblate_email_host_user,
  String $weblate_email_host_password,
  Array[String] $weblate_alternative_domains = [],
  String $weblate_admin_name = 'Weblate Admin',
  String $weblate_auto_update = 'none',
  String $weblate_email_host = '10.0.2.2',
  String $weblate_email_port = '25',
  String $weblate_email_use_tls = '0',
  String $weblate_email_use_ssl = '0',
  String $weblate_server_email = 'sysadmins@tails.net',
  String $weblate_default_from_email = 'weblate@tails.net',
  String $weblate_default_commiter_email = 'tails-l10n@boum.org',
  String $weblate_default_commiter_name = 'Tails translators',
  String $weblate_mt_tmserver = 'http://10.0.2.2:8888/tmserver/',
  Array[String] $weblate_remove_addons = [],  # See :/data/common.yaml for default
  Array[String] $weblate_remove_checks = [],  # values for these two parameters.
  String $postgres_host = '10.0.2.2',
  String $postgres_user = 'weblate',
  String $postgres_port = '5432',
  String $redis_host = '10.0.2.2',
  String $redis_port = '6379',
  String $redis_db = '1',
  Array[String] $extra_volumes = [],  # Used for testing purposes
) inherits weblate::params {
  include weblate::config
  include weblate::users_and_groups
  include weblate::scripts

  class { 'podman':
    manage_subuid            => true,
    # podman-docker is currently only available in Debian experimental repo
    podman_docker_pkg_ensure => 'absent',
  }

  # Application data needs to be accessible to:
  #
  #   1. the `weblate` host user that starts the container and is root inside
  #      of it; and
  #   2. the `weblate` container user used to run the application.
  #
  # To satisfy those, we do the following:
  #
  #   - Set the UID/GID of the (host) user/group `weblate` to a chosen value
  #     with enough slack for assigning a uid/gid namespace to it.
  #
  #   - Map a user namespace of subuids/subgids to the (host) `weblate` user to
  #     allow it to use a range of values inside the container.
  #
  #   - Set the owner of app data files/dirs to the UID that corresponds to the
  #     `weblate` user inside the container.
  #
  #   - Set the group of app data files/dirs to the GID that corresponds
  #     to the `weblate` group outside the container.
  #
  # This is an example of the mapping that we do:
  #
  #        | In the host                   | In the container     |
  #  , --- | ----------------------------- | -------------------- |
  #  | UID | 2001000 (in subuid namespace) | 1000 (weblate)       |
  #  | GID | 2000000 (weblate)             | 0    (root)          |
  #

  # Map a set of subuids and subgids to the selected user and group.

  $subuid_start = $system_uid + 1
  $subgid_start = $system_gid + 1

  podman::subuid { $system_user:
    subuid  => $subuid_start,
    count   => 65536,
    require => User[$system_user],
  }

  podman::subgid { $system_group:
    subgid  => $subgid_start,
    count   => 65536,
    require => Group[$system_group],
  }

  sysctl::value { 'kernel.unprivileged_userns_clone':
    value => 1,
  }

  ## Container configuration

  # Weblate requires a secret key in `/app/data/secret`.
  file { "${weblate_data_dir}/secret":
    ensure  => file,
    owner   => $ns_weblate_uid,
    group   => $ns_root_gid,
    content => $weblate_secret_key,
    require => File[$weblate_data_dir],
  }

  file { "${weblate_config_dir}/podman.env":
    ensure  => file,
    content => epp('weblate/podman.env.epp', {
        weblate_site_domain            => $weblate_site_domain,
        weblate_alternative_domains    => $weblate_alternative_domains,
        weblate_admin_name             => $weblate_admin_name,
        weblate_admin_password         => $weblate_admin_password,
        weblate_admin_email            => $weblate_admin_email,
        weblate_auto_update            => $weblate_auto_update,
        weblate_email_host             => $weblate_email_host,
        weblate_email_port             => $weblate_email_port,
        weblate_email_host_user        => $weblate_email_host_user,
        weblate_email_host_password    => $weblate_email_host_password,
        weblate_email_use_tls          => $weblate_email_use_tls,
        weblate_email_use_ssl          => $weblate_email_use_ssl,
        weblate_server_email           => $weblate_server_email,
        weblate_default_from_email     => $weblate_default_from_email,
        weblate_default_commiter_email => $weblate_default_commiter_email,
        weblate_default_commiter_name  => $weblate_default_commiter_name,
        weblate_mt_tmserver            => $weblate_mt_tmserver,
        weblate_remove_addons          => $weblate_remove_addons,
        weblate_remove_checks          => $weblate_remove_checks,
        postgres_database              => $postgres_database,
        postgres_host                  => $postgres_host,
        postgres_user                  => $postgres_user,
        postgres_password              => $postgres_password,
        postgres_port                  => $postgres_port,
        redis_host                     => $redis_host,
        redis_port                     => $redis_port,
        redis_db                       => $redis_db,
        redis_password                 => $redis_password,
    }),
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0640',  # contains passwords
    notify  => Podman::Container['weblate'],
    require => File[$weblate_config_dir],
  }

  file { "${weblate_data_dir}/settings-override.py":
    ensure  => file,
    source  => 'puppet:///modules/weblate/config/settings-override.py',
    owner   => $system_uid + 1000,  # Owner is `weblate` inside the container (UID 1000)
    group   => $system_gid,
    mode    => '0644',
    notify  => Podman::Container['weblate'],
    require => File[$weblate_data_dir],
  }

  podman::container { 'weblate':
    image   => "docker.io/weblate/weblate:${weblate_version}",
    user    => $system_user,
    flags   => {
      'publish'  => '8080:8080',
      'volume'   => [
        "${weblate_data_dir}:/app/data:z",
      ] + $extra_volumes,
      'env-file' => "${weblate_config_dir}/podman.env",
      # this container will be able to see the hosts localhost at 10.0.2.2
      'network'  => 'slirp4netns:allow_host_loopback=true',
    },
    require => [
      User[$system_user],
      Sysctl::Value['kernel.unprivileged_userns_clone'],
    ],
  }

  # We need Python's YAML module for some of our custom scripts that need to be
  # run inside the container. But the container itself doesn't include it, so
  # we link to a package installed on the host.
  #
  # XXX: remove this (and fix the run_in_container.sh script sccordingly) if
  #      ever (a) we stop depending on YAML or (b) a newer version of the
  #      Weblate container includes it.
  ensure_packages(['python3-yaml'])

  file { "${weblate_scripts_dir}/run_in_container.sh":
    ensure  => file,
    content => epp('weblate/run_in_container.sh.epp', {
        weblate_home        => $weblate_home,
        weblate_config_dir  => $weblate_config_dir,
        weblate_data_dir    => $weblate_data_dir,
        weblate_scripts_dir => $weblate_scripts_dir,
        weblate_repos_dir   => $weblate_repos_dir,
        weblate_logs_dir    => $weblate_logs_dir,
        weblate_version     => $weblate_version,
    }),
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0755',
    require => [
      File[$weblate_data_dir],
      File[$weblate_scripts_dir],
      Package['python3-yaml'],
    ],
  }

  # A service management script

  ensure_packages(['python3-click'])

  file { '/usr/local/bin/podman-weblate':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/weblate/podman-weblate.py',
    require => Package['python3-click'],
  }
}
